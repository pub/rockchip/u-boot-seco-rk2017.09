/*
 * (C) Copyright 2016 Rockchip Electronics Co., Ltd
 *
 * SPDX-License-Identifier:     GPL-2.0+
 */

/dts-v1/;
#include <dt-bindings/pwm/pwm.h>
#include <dt-bindings/pinctrl/rockchip.h>
#include "rk3399.dtsi"
#include "rk3399-sdram-lpddr3-4GB-1600.dtsi"
#include "rk3399-u-boot.dtsi"
#include <linux/media-bus-format.h>
#include <dt-bindings/input/input.h>

/ {
	model      = "Rockchip RK3399 SECO Board (C31)";
	compatible = "rockchip,seco-rk3399-c31", "rockchip,rk3399";


/*  __________________________________________________________________________
 * |                                                                          |
 * |                                 REGULATORS                               |
 * |__________________________________________________________________________|
 */
	sd_pwr: sd-pwr {
		compatible              = "regulator-fixed";
		gpio                    = <&gpio0 1 GPIO_ACTIVE_HIGH>;
		pinctrl-names           = "default";
		pinctrl-0               = <&sdmmc0_pwr_h>;
		regulator-name          = "vcc_sd";
		regulator-min-microvolt = <3300000>;
		regulator-max-microvolt = <3300000>;
		enable-active-high;
	};

        vcc_phy: vcc-phy-regulator {
                compatible = "regulator-fixed";
                regulator-name = "vcc_phy";
                regulator-always-on;
                regulator-boot-on;
        };

	vdd_en_regulator_cpub: vcc-en-cpub {
                compatible              = "regulator-fixed";
                pinctrl-names           = "default";
                pinctrl-0               = <&vsel1_gpio>;
                gpio                    = <&gpio1 RK_PC1 GPIO_ACTIVE_HIGH>;
                regulator-name          = "VDD_EN_CPUB";
                regulator-min-microvolt = <3300000>;
                regulator-max-microvolt = <3300000>;
                enable-active-low;
                regulator-always-on;
                regulator-boot-on;
        };

        vdd_en_regulator_gpu: vcc-en-gpu {
                compatible              = "regulator-fixed";
                pinctrl-names           = "default";
                pinctrl-0               = <&vsel2_gpio>;
                gpio                    = <&gpio1 RK_PB6 GPIO_ACTIVE_HIGH>;
                regulator-name          = "VDD_EN_GPU";
                regulator-min-microvolt = <3300000>;
                regulator-max-microvolt = <3300000>;
                enable-active-low;
                regulator-always-on;
                regulator-boot-on;
        };

        vccadc_ref: vccadc-ref {
                compatible = "regulator-fixed";
                regulator-name = "vcc1v8_sys";
                regulator-always-on;
                regulator-boot-on;
                regulator-min-microvolt = <1800000>;
                regulator-max-microvolt = <1800000>;
        };

};


/*  __________________________________________________________________________
 * |                                                                          |
 * |                                   ADC                                    |
 * |__________________________________________________________________________|
 */

&saradc {
        vref-supply = <&vccadc_ref>;
        status = "okay";
};

/*  __________________________________________________________________________
 * |                                                                          |
 * |                                   UART                                   |
 * |__________________________________________________________________________|
 */
&uart2 {
	clock-frequency = <14745600>;
	status = "okay";
};
/*  __________________________________________________________________________
 * |__________________________________________________________________________|
 */



/*  __________________________________________________________________________
 * |                                                                          |
 * |                              eMMC/uSD/SDIO                               |
 * |__________________________________________________________________________|
 */
 /*  eMMC  */
&emmc_phy {
	status = "okay";
};



/*  __________________________________________________________________________
 * |                                                                          |
 * |                                   Ethernet                               |
 * |__________________________________________________________________________|
 */
&gmac {
        phy-supply = <&vcc_phy>;
        phy-mode = "rgmii";
        clock_in_out = "output";
        snps,reset-gpio = <&gpio3 RK_PB7 GPIO_ACTIVE_LOW>;
        snps,reset-active-low;
        snps,reset-delays-us = <0 10000 50000>;
        assigned-clocks = <&cru SCLK_RMII_SRC>;
        assigned-clock-parents = <&cru SCLK_MAC>;
        assigned-clock-rates   = <125000000>;
        pinctrl-names = "default";
        pinctrl-0 = <&rgmii_pins>;
        tx_delay = <0x28>;
        rx_delay = <0x11>;
        status = "okay";
};

&sdhci {
	bus-width                    = <8>;
	mmc-hs400-1_8v;
	mmc-hs400-enhanced-strobe;
	non-removable;
	status                       = "okay";
};


/*  uSD  */
&sdmmc {
	bus-width = <4>;
	status    = "okay";
};
/*  __________________________________________________________________________
 * |__________________________________________________________________________|
 */


/*  __________________________________________________________________________
 * |                                                                          |
 * |                                   I2C                                    |
 * |__________________________________________________________________________|
 */
&i2c0 {
	i2c-scl-rising-time-ns  = <168>;
	i2c-scl-falling-time-ns = <4>;
	clock-frequency         = <400000>;
	status                  = "okay";

	rk808: pmic@1b {
		compatible                        = "rockchip,rk808";
		clock-output-names                = "xin32k", "wifibt_32kin";
		interrupt-parent                  = <&gpio1>;
		interrupts                        = <21 IRQ_TYPE_LEVEL_LOW>;
		pinctrl-names                     = "default";
		pinctrl-0                         = <&pmic_int_l>;
		reg                               = <0x1b>;
		rockchip,system-power-controller;
		#clock-cells                      = <1>;
		status                            = "okay";

		regulators {
		};
	};
};

&i2c4 {
        status = "okay";
	clock-frequency = <400000>;
};

/*  __________________________________________________________________________
 * |__________________________________________________________________________|
 */


&pinctrl {

	pmic {
		pmic_int_l: pmic-int-l {
			rockchip,pins =
				<1 RK_PC5 RK_FUNC_GPIO &pcfg_pull_up>;
		};
	};


	sdmmc0_pwr_h {
		sdmmc0_pwr_h: sdmmc0_pwr_h {
			rockchip,pins =
				<0 RK_PA1 RK_FUNC_GPIO &pcfg_pull_up>;
		};
	};

        vdd_regulator {
                vsel1_gpio: vsel1-gpio {
                        rockchip,pins = <1 RK_PC1 RK_FUNC_GPIO &pcfg_pull_down>;
                };

                vsel2_gpio: vsel2-gpio {
                        rockchip,pins = <1 RK_PB6 RK_FUNC_GPIO &pcfg_pull_down>;
                };
        };
};

/*  __________________________________________________________________________
 * |                                                                          |
 * |                                   USB2                                   |
 * |__________________________________________________________________________|
 */

&u2phy0 {
        status = "okay";

        u2phy0_otg: otg-port {
                status = "okay";
        };

        u2phy0_host: host-port {
                status = "okay";
        };
};

&usb_host0_ehci {
        status = "okay";
};

/*  __________________________________________________________________________
 * |                                                                          |
 * |                                   USB3                                   |
 * |__________________________________________________________________________|
 */

&dwc3_typec1 {
        status = "okay";
};
