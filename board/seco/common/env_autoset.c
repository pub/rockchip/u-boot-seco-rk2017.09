#include <common.h>
#include <asm/arch/sys_proto.h>
#include <linux/errno.h>
#include <asm/io.h>
#include <stdbool.h>
#include <mmc.h>
#include <i2c.h>
#include <dm/uclass-internal.h>


static int check_mmc_autodetect(void)
{
	char *autodetect_str = env_get("mmcautodetect");

	if ((autodetect_str != NULL) &&
		(strcmp(autodetect_str, "yes") == 0)) {
		return 1;
	}

	return 0;
}

/* This should be defined for each board */
__weak int mmc_map_to_kernel_blk(int dev_no)
{
	return dev_no;
}

void autoset_boot_device(void)
{
	char cmd[32];
	char mmcblk[32];
	u32 dev_no = mmc_get_env_dev();

	if (!check_mmc_autodetect())
		return;

	env_set_ulong("mmcdev", dev_no);

	env_set_ulong ("root_device_id", dev_no);
	env_set_ulong ("kernel_device_id", dev_no);
	env_set_ulong ("bootscript_device_id", dev_no);
	env_set_ulong ("fdt_device_id", dev_no);
	env_set_ulong ("ramfs_device_id", dev_no);

	/* Set mmcblk env */
	sprintf(mmcblk, "/dev/mmcblk%dp2 rootwait rw",
		mmc_map_to_kernel_blk(dev_no));
	env_set("mmcroot", mmcblk);

	sprintf(cmd, "mmc dev %d", dev_no);
	run_command(cmd, 0);

}

int get_macaddress_from_eeprom(struct udevice *i2c_bus, int i2c_addr, u8 *mac_addr)
{

	struct udevice *i2c_eeprom;
	int ret;

        ret = dm_i2c_probe(i2c_bus, i2c_addr, 0, &i2c_eeprom);
        if (ret) {
            printf("Failed to connect to EEPROM 0x%03x\n", i2c_addr);
            return -1;
        }

        ret = dm_i2c_read(i2c_eeprom, 0x00, mac_addr, 6);
	if (ret) {
            printf("Failed to connect to EEPROM 0x%03x\n", i2c_addr);
            return -1;
        }

        /* Only get MAC address, if eeprom is well programmed */ 
        if ((mac_addr[0] & mac_addr[1] & mac_addr[2] & mac_addr[3] & mac_addr[4] & mac_addr[5]) == 0xFF ||
                      (mac_addr[0] | mac_addr[1] | mac_addr[2] | mac_addr[3] | mac_addr[4] | mac_addr[5]) == 0x00 )
		return -1;
	
	return 0;
}
