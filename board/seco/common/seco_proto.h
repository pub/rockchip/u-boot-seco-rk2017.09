

#ifdef CONFIG_SECO_ENV_AUTOSET

extern void autoset_boot_device(void);
extern int get_macaddress_from_eeprom(struct udevice *i2c_bus, int i2c_addr, u8 *mac_addr);

#endif
