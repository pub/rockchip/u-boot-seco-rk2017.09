/*
 * (C) Copyright 2017 Rockchip Electronics Co., Ltd
 *
 * SPDX-License-Identifier:     GPL-2.0+
 */

#include <common.h>
#include <dm.h>
#include <asm/io.h>
#include <asm/gpio.h>
#include <asm/arch/param.h>
#include <stdlib.h>

#include <i2c.h>
#include <usb.h>
#include <dm/uclass-internal.h>

#include "../common/seco_proto.h"
#ifdef CONFIG_SECO_ENV_MANEGER
	#include <seco/env_common.h>
#endif 
#include "env_conf.h"

DECLARE_GLOBAL_DATA_PTR;

#ifndef CONFIG_SPL_BUILD

#define USB_TOUCH_VENDOR_ID     0x222a
#define USB_TOUCH_PRODUCT_ID    0x0001

#define I2C_BUS1	1
#define I2C_SPEED	400000

#define I2C_TOUCH_LVDS_ADDR 92
#define I2C_IO_EXP_A 0x20
#define I2C_EEPROM_REVB 0x50
#define I2C_EEPROM_REVC 0x52
#define I2C_EEPROM_REVC1 0x53

#define I2C_IO_EXPANDER_REG_2 0x2
#define I2C_IO_EXPANDER_REG_3 0x3
#define I2C_IO_EXPANDER_REG_6 0x6
#define I2C_IO_EXPANDER_REG_7 0x7

bool is_usb_device_present(unsigned long vendor, unsigned long product);
static bool is_device_present(struct usb_device *udev, unsigned long vendor, unsigned long product);
/**************************************
*  Board code:
*     0xKYZZ
*        |||
*        |ZZ => 00 = RevA;
*        |      01 = RevB;
*        |      02 = RevC;
*        |      03 = RevC1;
*        |
*        Y => 0 = HDMI;
*             1 = LVDS;
**************************************/

static int board_code(void) {
    int eeprom_addr, chip_is_present;
    int rev_code = 0x0;
    int board_code = 0x0;
    int display_code = 0x0;
	struct udevice *i2c_bus, *i2c_chip;

    uclass_get_device_by_name(UCLASS_I2C, "i2c@ff190000", &i2c_bus);
	dm_i2c_set_bus_speed(i2c_bus, I2C_SPEED);
    for (eeprom_addr = 0x50; eeprom_addr < 0x58; eeprom_addr++) {
      if (eeprom_addr != 0x51) {
        chip_is_present = dm_i2c_probe(i2c_bus, eeprom_addr, 0, &i2c_chip);
        if (chip_is_present == 0)
            break;
      }
      rev_code++;
    }
    chip_is_present = dm_i2c_probe(i2c_bus, I2C_TOUCH_LVDS_ADDR, 0, &i2c_chip);
    if (chip_is_present == 0)
        display_code = 0x1;

    board_code = rev_code | (display_code << 8);

    return board_code;
}

static void setup_seco_environment(void)
{
#ifdef CONFIG_SECO_ENV_MANEGER
	gd->bsp_sources.kern_dev_list        = &kern_dev_list[0];
	gd->bsp_sources.kern_dev_num         = kern_dev_size;
	gd->bsp_sources.fdt_dev_list         = &fdt_dev_list[0];
	gd->bsp_sources.fdt_dev_num          = fdt_dev_size;
#ifdef CONFIG_OF_LIBFDT_OVERLAY
	gd->bsp_sources.fdt_overlay_dev_list     = fdt_overlay_dev_list;
	gd->bsp_sources.fdt_overlay_dev_num      = fdt_overlay_dev_size;
#endif
	gd->bsp_sources.ramfs_dev_list       = &ramfs_dev_list[0];
	gd->bsp_sources.ramfs_dev_num        = ramfs_dev_size;
	gd->bsp_sources.filesystem_dev_list  = &filesystem_dev_list[0];
	gd->bsp_sources.filesystem_dev_num   = filesystem_dev_size;

	gd->boot_setup.video_mode_list           = video_mode_list;
	gd->boot_setup.video_mode_num            = video_mode_size;
#endif
}
#endif

#ifndef CONFIG_SPL_BUILD
int mmc_get_boot_dev(void) {
	int ret;
	char *devtype = NULL, *devnum = NULL;
	ret = param_parse_bootdev(&devtype, &devnum);
	if (!ret) {
        	return atoi(devnum);
	} else {
		return 0;
	}
}

void get_board_serial(void)
{
    return;
}

static int setup_macaddr(void)
{
#if CONFIG_IS_ENABLED(CMD_NET)
	struct udevice *i2c_bus, *i2c_eeprom;
    int ret;
	u8 mac_addr[6];
    uclass_get_device_by_name(UCLASS_I2C, "i2c@ff190000", &i2c_bus);
    ret = dm_i2c_probe(i2c_bus, I2C_EEPROM_REVC, 0, &i2c_eeprom);
    if (ret) {
        printf("Failed to connect to EEPROM_REVC 0x%02x\n", I2C_EEPROM_REVC);
        if (ret) {
            ret = dm_i2c_probe(i2c_bus, I2C_EEPROM_REVC1, 0, &i2c_eeprom);
        } else {
            printf("Failed to connect to EEPROM_REVC and EEPROM_REVC1 0x%02x\n", I2C_EEPROM_REVC1);
            return -ENODEV;
        }
    }

    ret = dm_i2c_read(i2c_eeprom, 0x00, mac_addr, 6);

	/* Only set a MAC address, if eeprom is well programmed */
	if ((mac_addr[0] & mac_addr[1] & mac_addr[2] & mac_addr[3] & mac_addr[4] & mac_addr[5]) == 0xFF)
		return -1;

	/* Only set a MAC address, if eeprom is well programmed */
	if ((mac_addr[0] | mac_addr[1] | mac_addr[2] | mac_addr[3] | mac_addr[4] | mac_addr[5]) == 0x00)
		return -1;

	/* Only set a MAC address, if none is set in the environment */
//	if (env_get("ethaddr"))
//		return -1;

	/* Copy 6 bytes of the hash to base the MAC address on */

	/* Make this a valid MAC address and set it */
//	mac_addr[0] &= 0xfe;  /* clear multicast bit */
//	mac_addr[0] |= 0x02;  /* set local assignment bit (IEEE802) */
	eth_env_set_enetaddr("ethaddr", mac_addr);
#endif

	return 0;
}

int misc_init_r(void)
{
    get_board_serial();
#ifndef CONFIG_SPL_BUILD
#ifdef CONFIG_SECO_ENV_AUTOSET
	autoset_boot_device();
#endif
	setup_seco_environment();
	setup_macaddr();
#endif
    return 0;
}

void set_board_environment(void)
{
    int boardcode;
    boardcode = board_code();
    char *board_part = "px30-d23";
    char rev_part[8];
    char video_part[8];
    char custom_dts_str[64];

    switch (boardcode & 0xFF) {
//        case 0x0:
//            printf("RevA\n");
//            env_set("rev_part", "-reva");
//            sprintf(rev_part, "reva");
//            break;
        case 0x0:
        case 0x1:
            printf("RevB\n");
            env_set("rev_part", "-revb");
            sprintf(rev_part, "revb");
            break;
        case 0x2:
            printf("RevC\n");
            env_set("rev_part", "-revc");
            sprintf(rev_part, "revc");
            break;
        case 0x3:
            printf("RevC1\n");
            env_set("rev_part", "-revc1");
            sprintf(rev_part, "revc1");
            break;
        default:
            printf("RevC1\n");
            env_set("rev_part", "-revc1");
            sprintf(rev_part, "revc1");
            break;
    }

    if (is_usb_device_present(USB_TOUCH_VENDOR_ID, USB_TOUCH_PRODUCT_ID)) {
        env_set("video_part", "-lvds");
        sprintf(video_part, "lvds");
    } else {
        env_set("video_part", "-hdmi");
        sprintf(video_part, "hdmi");
    }

//    printf("Using dtb file = seco-%s-%s-%s.dtb\n", board_part, rev_part, video_part);
//    sprintf(custom_dts_str, "seco-%s-%s-%s.dtb", board_part, rev_part, video_part);
    printf("Using dtb file = seco-%s-%s.dtb\n", board_part, rev_part);
    sprintf(custom_dts_str, "seco-%s-%s.dtb", board_part, rev_part);
    env_set("custom_fdt_file", custom_dts_str);
    env_set("fdt_file", custom_dts_str);

    

#ifndef CONFIG_SPL_BUILD
	setup_seco_environment();
#endif
	return;
}

void ioexpander_init(void)
{
#if REVB
	struct gpio_desc gpio1_8, gpio1_11;
	struct udevice *gpio_dev1 = NULL;
    printf("IO Expander Setup.\n");
	uclass_get_device_by_name(UCLASS_GPIO, "gpio1@ff250000", &gpio_dev1);
    gpio1_8.dev = gpio_dev1;
    gpio1_8.offset = 8;
    gpio1_8.flags = 0;
    gpio1_11.dev = gpio_dev1;
    gpio1_11.offset = 11;
    gpio1_11.flags = 0;
    dm_gpio_request(&gpio1_8, "io_expanderA_reset");
    dm_gpio_request(&gpio1_11, "io_expanderB_reset");
	dm_gpio_set_dir_flags(&gpio1_8, GPIOD_IS_OUT);
	dm_gpio_set_dir_flags(&gpio1_11, GPIOD_IS_OUT);
    udelay(100);
    dm_gpio_set_value(&gpio1_8, 1);
    dm_gpio_set_value(&gpio1_11, 1);
#else
	struct gpio_desc gpio1_8, gpio0_14;
	struct udevice *gpio_dev0 = NULL;
	struct udevice *gpio_dev1 = NULL;
    printf("IO Expander Setup iiii.\n");
	uclass_get_device_by_name(UCLASS_GPIO, "gpio0@ff040000" , &gpio_dev0);
	uclass_get_device_by_name(UCLASS_GPIO, "gpio1@ff250000", &gpio_dev1);
    gpio1_8.dev = gpio_dev1;
    gpio1_8.offset = 8;
    gpio1_8.flags = 0;
    gpio0_14.dev = gpio_dev0;
    gpio0_14.offset = 14;
    gpio0_14.flags = 0;
    dm_gpio_request(&gpio1_8, "io_expanderA_reset");
    dm_gpio_request(&gpio0_14, "io_expanderB_reset");
	dm_gpio_set_dir_flags(&gpio1_8, GPIOD_IS_OUT);
	dm_gpio_set_dir_flags(&gpio0_14, GPIOD_IS_OUT);
    udelay(100);
    dm_gpio_set_value(&gpio1_8, 1);
    dm_gpio_set_value(&gpio0_14, 1);
    udelay(100);
#endif
}

int d23_usb_init(void)
{
	struct udevice *i2c_bus, *i2c_exp_a;
    int ret;
    u8 buf[2];
    uclass_get_device_by_name(UCLASS_I2C, "i2c@ff190000", &i2c_bus);
    ret = dm_i2c_probe(i2c_bus, I2C_IO_EXP_A, 0, &i2c_exp_a);
    if (ret) {
        printf("Failed to connect to IO EXPANDER A\n");
        return -ENODEV;
    }

    buf[0] = 0xC3;
    ret = dm_i2c_write(i2c_exp_a, I2C_IO_EXPANDER_REG_2, buf, 1);
    ret = dm_i2c_write(i2c_exp_a, I2C_IO_EXPANDER_REG_6, buf, 1);

    buf[0] = 0xFB;
    ret = dm_i2c_write(i2c_exp_a, I2C_IO_EXPANDER_REG_3, buf, 1);
    ret = dm_i2c_write(i2c_exp_a, I2C_IO_EXPANDER_REG_7, buf, 1);

    printf("IO EXPANDER A initialization done. [%d]\n", ret);
    return ret;
}

static bool is_mass_storage_present(struct usb_device *udev)
{
	struct udevice *child;

	if (udev->descriptor.bDeviceClass == 0)
      if (udev->config.if_desc[0].desc.bInterfaceClass == USB_CLASS_MASS_STORAGE)
            return true;  

	for (device_find_first_child(udev->dev, &child);
	     child;
	     device_find_next_child(&child)) {
		if (device_active(child) &&
		    (device_get_uclass_id(child) != UCLASS_USB_EMUL) &&
		    (device_get_uclass_id(child) != UCLASS_BLK)) {
			udev = dev_get_parent_priv(child);
			if (is_mass_storage_present(udev))
              return true;
		}
	}
    return false;  
}

bool is_usb_mass_storage_present(void)
{
    struct udevice *bus;

    for (uclass_find_first_device(UCLASS_USB, &bus);
        bus;
        uclass_find_next_device(&bus)) {
        struct usb_device *udev;
        struct udevice *dev;

        if (!device_active(bus))
            continue;

        device_find_first_child(bus, &dev);
        if (dev && device_active(dev)) {
            udev = dev_get_parent_priv(dev);
            if (is_mass_storage_present(udev))
              return true;
        }
    }
    return false;
}

static bool is_device_present(struct usb_device *udev, unsigned long vendor, unsigned long product)
{
	struct udevice *child;

	if (udev->descriptor.bDeviceClass == 0)
      if ((udev->descriptor.idVendor == vendor) && 
          (udev->descriptor.idProduct == product))
            return true;  

	for (device_find_first_child(udev->dev, &child);
	     child;
	     device_find_next_child(&child)) {
		if (device_active(child) &&
		    (device_get_uclass_id(child) != UCLASS_USB_EMUL) &&
		    (device_get_uclass_id(child) != UCLASS_BLK)) {
			udev = dev_get_parent_priv(child);
			if (is_device_present(udev, vendor, product))
              return true;
		}
	}
    return false;  
}

bool is_usb_device_present(unsigned long vendor, unsigned long product)
{
    struct udevice *bus;

    for (uclass_find_first_device(UCLASS_USB, &bus);
        bus;
        uclass_find_next_device(&bus)) {
        struct usb_device *udev;
        struct udevice *dev;

        if (!device_active(bus))
            continue;

        device_find_first_child(bus, &dev);
        if (dev && device_active(dev)) {
            udev = dev_get_parent_priv(dev);
            if (is_device_present(udev, vendor, product))
              return true;
        }
    }
    return false;
}

int rk_board_late_init(void)
{
    ioexpander_init();
    d23_usb_init();
    usb_init();
    set_board_environment();
    if (is_usb_mass_storage_present())
        env_set("bootusb", "true");
      
    return 0;
}
#endif

#ifndef CONFIG_SPL_BUILD
#if defined(CONFIG_ENV_IS_IN_MMC)
int mmc_get_env_dev(void) {
	int ret;
	char *devtype = NULL, *devnum = NULL;
	ret = param_parse_bootdev(&devtype, &devnum);
	if (!ret) {
        	return atoi(devnum);
	} else {
		return 0;
	}
}
#endif
#endif
