#include <common.h>

#include <seco/env_common.h>

#define BOOT_DEV_ID_EMMC      __stringify(BOOT_ID_EMMC)"\0"
#define BOOT_DEV_ID_U_SD      __stringify(BOOT_ID_USD)"\0"
#define BOOT_DEV_ID_EXT_SD    __stringify(BOOT_ID_EXT_SD)"\0"
#define BOOT_DEV_ID_SPI       "0"
#define BOOT_DEV_ID_SATA      "0"
#define BOOT_DEV_ID_USB       "0"

#define ROOT_DEV_ID_EMMC      __stringify(ROOT_ID_EMMC)"\0"
#define ROOT_DEV_ID_U_SD      __stringify(ROOT_ID_USD)"\0"


#define LOAD_ADDR_KERNEL_LOCAL_DEV    __stringify(ENV_KERNEL_LOADADDR)"\0"
#define LOAD_ADDR_KERNEL_REMOTE_DEV   __stringify(ENV_KERNEL_LOADADDR)"\0"

#define LOAD_ADDR_FDT_LOCAL_DEV       __stringify(ENV_FDT_LOADADDR)"\0"
#define LOAD_ADDR_FDT_REMOTE_DEV      __stringify(ENV_FDT_LOADADDR)"\0"

#define LOAD_ADDR_RAMFS_LOCAL_DEV       __stringify(ENV_RAMFS_LOADADDR)"\0"
#define LOAD_ADDR_RAMFS_REMOTE_DEV      __stringify(ENV_RAMFS_LOADADDR)"\0"

#define LOAD_ADDR_FDT_OVERLAY_LOCAL_DEV       __stringify(ENV_FDT_OVERLAY_LOADADDR)"\0"
#define LOAD_ADDR_FDT_OVERLAY_REMOTE_DEV      __stringify(ENV_FDT_OVERLAY_LOADADDR)"\0"

#define __STR(X) #X
#define STR(X) __STR(X)


data_boot_dev_t kern_dev_list [] = {
        { DEV_EMMC,     "eMMC onboard",   STR(MACRO_ENV_KERNEL_SRC_USDHCI),    BOOT_DEV_ID_EMMC,    LOAD_ADDR_KERNEL_LOCAL_DEV,   "Image" },
        { DEV_U_SD,     "uSD onboard",    STR(MACRO_ENV_KERNEL_SRC_USDHCI),    BOOT_DEV_ID_U_SD,    LOAD_ADDR_KERNEL_LOCAL_DEV,   "Image" },
        { DEV_TFTP,     "TFTP",           STR(MACRO_ENV_KERNEL_SRC_TFTP),      "",                  LOAD_ADDR_KERNEL_REMOTE_DEV,  "Image" },
        { DEV_USB,      "USB",            STR(MACRO_ENV_KERNEL_SRC_USB),       BOOT_DEV_ID_USB,     LOAD_ADDR_KERNEL_LOCAL_DEV,   "Image" },
};

size_t kern_dev_size = sizeof( kern_dev_list ) / sizeof( kern_dev_list[0] );


data_boot_dev_t fdt_dev_list [] = {
        { DEV_EMMC,     "eMMC onboard",   STR(MACRO_ENV_FDT_SRC_USDHCI),     BOOT_DEV_ID_EMMC,    LOAD_ADDR_FDT_LOCAL_DEV,   CONFIG_DEFAULT_FDT_FILE },
        { DEV_U_SD,     "uSD onboard",    STR(MACRO_ENV_FDT_SRC_USDHCI),     BOOT_DEV_ID_U_SD,    LOAD_ADDR_FDT_LOCAL_DEV,   CONFIG_DEFAULT_FDT_FILE },
        { DEV_TFTP,     "TFTP",           STR(MACRO_ENV_FDT_SRC_TFTP),       "",                  LOAD_ADDR_FDT_REMOTE_DEV,  CONFIG_DEFAULT_FDT_FILE },
        { DEV_USB,      "USB",            STR(MACRO_ENV_FDT_SRC_USB),        BOOT_DEV_ID_USB,     LOAD_ADDR_FDT_LOCAL_DEV,   CONFIG_DEFAULT_FDT_FILE },
};

size_t fdt_dev_size = sizeof( fdt_dev_list ) / sizeof( fdt_dev_list[0] );


#ifdef CONFIG_OF_LIBFDT_OVERLAY
data_boot_dev_t fdt_overlay_dev_list [] = {
        { DEV_EMMC,     "eMMC onboard",   STR(MACRO_ENV_FDT_OVERLAY_SRC_USDHCI),  BOOT_DEV_ID_EMMC,    LOAD_ADDR_FDT_OVERLAY_LOCAL_DEV,   DEFAULT_OVERLAY_FILE },
        { DEV_U_SD,     "uSD onboard",    STR(MACRO_ENV_FDT_OVERLAY_SRC_USDHCI),  BOOT_DEV_ID_U_SD,    LOAD_ADDR_FDT_OVERLAY_LOCAL_DEV,   DEFAULT_OVERLAY_FILE },
        { DEV_TFTP,     "TFTP",           STR(MACRO_ENV_FDT_OVERLAY_SRC_TFTP),    "",                  LOAD_ADDR_FDT_OVERLAY_REMOTE_DEV,  DEFAULT_OVERLAY_FILE },
        { DEV_USB,      "USB",            STR(MACRO_ENV_FDT_OVERLAY_SRC_USB),     BOOT_DEV_ID_USB,     LOAD_ADDR_FDT_OVERLAY_LOCAL_DEV,   DEFAULT_OVERLAY_FILE },
};

size_t fdt_overlay_dev_size = sizeof( fdt_overlay_dev_list ) / sizeof( fdt_overlay_dev_list[0] );
#endif


data_boot_dev_t ramfs_dev_list [] = {
        { DEV_NONE,     "Not use",        "0x0",                              "0",                 "",                          ""},
        { DEV_EMMC,     "eMMC onboard",   STR(MACRO_ENV_RAMFS_SRC_USDHCI),    BOOT_DEV_ID_EMMC,    LOAD_ADDR_RAMFS_LOCAL_DEV,   "ramfs.img" },
        { DEV_U_SD,     "uSD onboard",    STR(MACRO_ENV_RAMFS_SRC_USDHCI),    BOOT_DEV_ID_U_SD,    LOAD_ADDR_RAMFS_LOCAL_DEV,   "ramfs.img" },
        { DEV_TFTP,     "TFTP",           STR(MACRO_ENV_RAMFS_SRC_TFTP),      "",                  LOAD_ADDR_RAMFS_REMOTE_DEV,  "ramfs.img" },
        { DEV_USB,      "USB",            STR(MACRO_ENV_RAMFS_SRC_USB),       BOOT_DEV_ID_USB,     LOAD_ADDR_RAMFS_LOCAL_DEV,   "ramfs.img" },
};

size_t ramfs_dev_size = sizeof( ramfs_dev_list ) / sizeof( ramfs_dev_list[0] );


data_boot_dev_t filesystem_dev_list [] = {
        { DEV_EMMC,     "eMMC onboard",   STR(MACRO_ENV_FS_SRC_USDHCI),     ROOT_DEV_ID_EMMC,    "" },
        { DEV_U_SD,     "uSD onboard",    STR(MACRO_ENV_FS_SRC_USDHCI),     ROOT_DEV_ID_U_SD,    "" },
        { DEV_NFS,      "NFS",            STR(MACRO_ENV_FS_SRC_NFS),        "",                  "" },
        { DEV_USB,      "USB",            STR(MACRO_ENV_FS_SRC_USB),        "",                  "" },
};

size_t filesystem_dev_size = sizeof( filesystem_dev_list ) / sizeof( filesystem_dev_list[0] );



video_mode_t video_mode_list [] = {
        {   
                .label    = "no display",
                .video = { 
                        { VIDEO_NOT_USED, VIDEO_LVDS, NO_VIDEO_ARGS },
                        { VIDEO_NOT_USED, VIDEO_HDMI, NO_VIDEO_ARGS },
                        { VIDEO_NOT_USED, VIDEO_EDP, NO_VIDEO_ARGS },
                        { VIDEO_NOT_USED, VIDEO_TYPEC, NO_VIDEO_ARGS },
                },
                .panel_name = "none",
                .dtbo_conf_file = NULL,
                .use_bootargs = 0,
        }, {   
                .label    = "LVDS",
                .video = { 
                        { VIDEO_USED, VIDEO_LVDS, NO_VIDEO_ARGS },
                        { VIDEO_NOT_USED, VIDEO_HDMI, NO_VIDEO_ARGS },
                        { VIDEO_NOT_USED, VIDEO_EDP, NO_VIDEO_ARGS },
                        { VIDEO_NOT_USED, VIDEO_TYPEC, NO_VIDEO_ARGS },
                },
                .panel_name = "none",
                .dtbo_conf_file = "seco-rk3399-c31-sn65dsi84.dtbo",
                .use_bootargs = 0,
        }, {   
                .label    = "HDMI",
                .video = { 
                        { VIDEO_NOT_USED, VIDEO_LVDS, NO_VIDEO_ARGS },
                        { VIDEO_USED, VIDEO_HDMI, NO_VIDEO_ARGS },
                        { VIDEO_NOT_USED, VIDEO_EDP, NO_VIDEO_ARGS },
                        { VIDEO_NOT_USED, VIDEO_TYPEC, NO_VIDEO_ARGS },
                },
                .panel_name = "none",
                .dtbo_conf_file = "seco-rk3399-c31-hdmi.dtbo",
                .use_bootargs = 0,
        }, {   
                .label    = "eDP",
                .video = { 
                        { VIDEO_NOT_USED, VIDEO_LVDS, NO_VIDEO_ARGS },
                        { VIDEO_NOT_USED, VIDEO_HDMI, NO_VIDEO_ARGS },
                        { VIDEO_USED, VIDEO_EDP, NO_VIDEO_ARGS },
                        { VIDEO_NOT_USED, VIDEO_TYPEC, NO_VIDEO_ARGS },
                },
                .panel_name = "none",
                .dtbo_conf_file = "seco-rk3399-c31-edp.dtbo",
                .use_bootargs = 0,
        }, {   
                .label    = "typeC",
                .video = { 
                        { VIDEO_NOT_USED, VIDEO_LVDS, NO_VIDEO_ARGS },
                        { VIDEO_NOT_USED, VIDEO_HDMI, NO_VIDEO_ARGS },
                        { VIDEO_NOT_USED, VIDEO_EDP, NO_VIDEO_ARGS },
                        { VIDEO_USED, VIDEO_TYPEC, NO_VIDEO_ARGS },
                },
                .panel_name = "none",
                .dtbo_conf_file = "seco-rk3399-c31-typec-dp.dtbo",
                .use_bootargs = 0,
        }, {   
                .label    = "HDMI+LVDS",
                .video = { 
                        { VIDEO_NOT_USED, VIDEO_LVDS, NO_VIDEO_ARGS },
                        { VIDEO_USED, VIDEO_HDMI, NO_VIDEO_ARGS },
                        { VIDEO_NOT_USED, VIDEO_EDP, NO_VIDEO_ARGS },
                        { VIDEO_NOT_USED, VIDEO_TYPEC, NO_VIDEO_ARGS },
                },
                .panel_name = "none",
                .dtbo_conf_file = "seco-rk3399-c31-hdmi-lvds.dtbo",
                .use_bootargs = 0,

        }, {   
                .label    = "HDMI+TYPEC DP",
                .video = { 
                        { VIDEO_NOT_USED, VIDEO_LVDS, NO_VIDEO_ARGS },
                        { VIDEO_USED, VIDEO_HDMI, NO_VIDEO_ARGS },
                        { VIDEO_NOT_USED, VIDEO_EDP, NO_VIDEO_ARGS },
                        { VIDEO_NOT_USED, VIDEO_TYPEC, NO_VIDEO_ARGS },
                },
                .panel_name = "none",
                .dtbo_conf_file = "seco-rk3399-c31-hdmi-typec-dp.dtbo",
                .use_bootargs = 0,
        },  
};

size_t video_mode_size = sizeof( video_mode_list ) / sizeof( video_mode_list[0] );


#ifdef CONFIG_OF_LIBFDT_OVERLAY
/* *********************************** FDT OVERLAY *********************************** */

overlay_list_t overlay_peripheral_list [] = {
	{
		.title = "SPI1 device",
		.options = { 
			{ "not use", "" },		// default
			{ "can1", "seco-rk3399-c31-can1.dtbo" },
			{ "eeprom", "seco-rk3399-c31-spi-eeprom.dtbo" },
		},
	}, {
		.title = "SPI2 device",
		.options = { 
			{ "not use", "" },		// default
			{ "can2", "seco-rk3399-c31-can2.dtbo" },
			{ "rtc", "seco-rk3399-c31-spi-rtc.dtbo" },
		},
	}, {
		.title = "Camera csi1",
		.options = { 
			{ "not use", "" },		// default
			{ "csi1", "seco-rk3399-c31-csi1.dtbo" },
		},

	}, {
		.title = "Camera csi2",
		.options = { 
			{ "not use", "" },		// default
			{ "csi2", "seco-rk3399-c31-csi2.dtbo" },
		},
	}, {
		.title = "RS485 serial",
		.options = { 
			{ "rs232", "" },		// default
			{ "rs485", "seco-rk3399-c31-rs485.dtbo" },
			{ "gpios", "seco-rk3399-c31-gpio.dtbo" },
		},
	},
};

size_t overlay_peripheral_size = sizeof( overlay_peripheral_list ) / sizeof( overlay_peripheral_list[0] );


#endif  /* CONFIG_OF_LIBFDT_OVERLAY */
