/*
 * (C) Copyright 2016 Rockchip Electronics Co., Ltd
 *
 * SPDX-License-Identifier:     GPL-2.0+
 */

#include <common.h>
#include <stdlib.h>
#include <dm.h>
#include <misc.h>
#include <ram.h>
#include <asm/arch/param.h>
#include <dm/pinctrl.h>
#include <dm/uclass-internal.h>
#include <asm/setup.h>
#include <asm/arch/periph.h>
#include <power/regulator.h>
#include <u-boot/sha256.h>
#include <usb.h>
#include <dwc3-uboot.h>
#include <spl.h>
#include <asm/gpio.h>

#include <i2c.h>
#include <dm/uclass-internal.h>

#include "../common/seco_proto.h"
#ifdef CONFIG_SECO_ENV_MANEGER
	#include <seco/env_common.h>
	#include "env_conf.h"
#endif 

#include "rk3399_c31_strap_cfg.h"


DECLARE_GLOBAL_DATA_PTR;

#define RK3399_CPUID_OFF  0x7
#define RK3399_CPUID_LEN  0x10

int rk_board_init(void)
{
	struct udevice *pinctrl, *regulator;
	struct udevice *i2c_bus,*i2c_pmic;
	int ret, i2c_addr = 0x1b;
	u8 buf = 0xf;

        /* retrieve pmic address from dts */
        uclass_get_device_by_name(UCLASS_I2C, "i2c@ff3c0000", &i2c_bus);

	ret = dm_i2c_probe(i2c_bus, i2c_addr, 0, &i2c_pmic);
        if (ret) {
            printf("Failed to connect to PMIC 0x%03x\n", i2c_addr);
            goto out;
        }
	/* Set LDO4 to 3V3 */
	ret = dm_i2c_write(i2c_pmic, 0x41, &buf , 1);
        if (ret) {
            printf("Failed to connect to write to PMIC 0xf\n");
            goto out;
        }

	/*
	 * The PWM does not have decicated interrupt number in dts and can
	 * not get periph_id by pinctrl framework, so let's init them here.
	 * The PWM2 and PWM3 are for pwm regulators.
	 */
	ret = uclass_get_device(UCLASS_PINCTRL, 0, &pinctrl);
	if (ret) {
		debug("%s: Cannot find pinctrl device\n", __func__);
		goto out;
	}

	/* Enable pwm0 for panel backlight */
	ret = pinctrl_request_noflags(pinctrl, PERIPH_ID_PWM0);
	if (ret) {
		debug("%s PWM0 pinctrl init fail! (ret=%d)\n", __func__, ret);
		goto out;
	}

	ret = pinctrl_request_noflags(pinctrl, PERIPH_ID_PWM2);
	if (ret) {
		debug("%s PWM2 pinctrl init fail!\n", __func__);
		goto out;
	}

	ret = pinctrl_request_noflags(pinctrl, PERIPH_ID_PWM3);
	if (ret) {
		debug("%s PWM3 pinctrl init fail!\n", __func__);
		goto out;
	}

	ret = regulator_get_by_platname("vcc5v0_host", &regulator);
	if (ret) {
		debug("%s vcc5v0_host init fail! ret %d\n", __func__, ret);
		goto out;
	}

	ret = regulator_set_enable(regulator, true);
	if (ret) {
		debug("%s vcc5v0-host-en set fail!\n", __func__);
                goto out;
        }

out:
	return 0;
}

#define I2C_EEPROM_ADDR		0x53

static void setup_macaddr(void)
{
#if CONFIG_IS_ENABLED(CMD_NET)
	int ret;
	const char *cpuid = env_get("cpuid#");
	u8 hash[SHA256_SUM_LEN];
	int size = sizeof(hash);
	u8 mac_addr[6], curr_mac_addr[6];
	struct udevice *i2c_bus;

	/* retrieve macaddress from eeprom */
	uclass_get_device_by_name(UCLASS_I2C, "i2c@ff3d0000", &i2c_bus);
	if(get_macaddress_from_eeprom(i2c_bus, I2C_EEPROM_ADDR, &mac_addr[0]) < 0) {
		printf("failed to get eeprom macaddress\n");

		if (!cpuid) {
			debug("%s: could not retrieve 'cpuid#'\n", __func__);
			return;
		}

		ret = hash_block("sha256", (void *)cpuid, strlen(cpuid), hash, &size);
		if (ret) {
			debug("%s: failed to calculate SHA256\n", __func__);
			return;
		}

		/* Copy 6 bytes of the hash to base the MAC address on */
		memcpy(mac_addr, hash, 6);

		/* Make this a valid MAC address and set it */
		mac_addr[0] &= 0xfe;  /* clear multicast bit */
		mac_addr[0] |= 0x02;  /* set local assignment bit (IEEE802) */

	}
	
	/* Get macaddress from bootargs and 
	 * overwrite it only if it is not a Seco macaddress 
	 */
	eth_env_get_enetaddr("ethaddr", &curr_mac_addr[0]);
	if(!(curr_mac_addr[0] == 0x0 && curr_mac_addr[1] == 0xc0 && curr_mac_addr[2] == 0x8))
		env_set("ethaddr","");
	eth_env_set_enetaddr("ethaddr", mac_addr);

#endif

	return;
}

static void setup_serial(void)
{
#if CONFIG_IS_ENABLED(ROCKCHIP_EFUSE)
	struct udevice *dev;
	int ret, i;
	u8 cpuid[RK3399_CPUID_LEN];
	u8 low[RK3399_CPUID_LEN/2], high[RK3399_CPUID_LEN/2];
	char cpuid_str[RK3399_CPUID_LEN * 2 + 1];
	u64 serialno;
	char serialno_str[16];

	/* retrieve the device */
	ret = uclass_get_device_by_driver(UCLASS_MISC,
					  DM_GET_DRIVER(rockchip_efuse), &dev);
	if (ret) {
		debug("%s: could not find efuse device\n", __func__);
		return;
	}

	/* read the cpu_id range from the efuses */
	ret = misc_read(dev, RK3399_CPUID_OFF, &cpuid, sizeof(cpuid));
	if (ret) {
		debug("%s: reading cpuid from the efuses failed\n",
		      __func__);
		return;
	}

	memset(cpuid_str, 0, sizeof(cpuid_str));
	for (i = 0; i < 16; i++)
		sprintf(&cpuid_str[i * 2], "%02x", cpuid[i]);

	debug("cpuid: %s\n", cpuid_str);

	/*
	 * Mix the cpuid bytes using the same rules as in
	 *   ${linux}/drivers/soc/rockchip/rockchip-cpuinfo.c
	 */
	for (i = 0; i < 8; i++) {
		low[i] = cpuid[1 + (i << 1)];
		high[i] = cpuid[i << 1];
	}

	serialno = crc32_no_comp(0, low, 8);
	serialno |= (u64)crc32_no_comp(serialno, high, 8) << 32;
	snprintf(serialno_str, sizeof(serialno_str), "%llx", serialno);

	env_set("cpuid#", cpuid_str);
	env_set("serial#", serialno_str);
#endif

	return;
}


#ifndef CONFIG_SPL_BUILD
int mmc_get_boot_dev(void) {
	int ret;
	char *devtype = NULL, *devnum = NULL;
	ret = param_parse_bootdev(&devtype, &devnum);
	if (!ret) {
        	return atoi(devnum);
	} else {
		return 0;
	}
}


static void setup_seco_environment(void)
{
#ifdef CONFIG_SECO_ENV_MANEGER
	gd->bsp_sources.kern_dev_list            = &kern_dev_list[0];
	gd->bsp_sources.kern_dev_num             = kern_dev_size;
	gd->bsp_sources.fdt_dev_list             = &fdt_dev_list[0];
	gd->bsp_sources.fdt_dev_num              = fdt_dev_size;
#ifdef CONFIG_OF_LIBFDT_OVERLAY
	gd->bsp_sources.fdt_overlay_dev_list     = fdt_overlay_dev_list;
	gd->bsp_sources.fdt_overlay_dev_num      = fdt_overlay_dev_size;
#endif
	gd->bsp_sources.ramfs_dev_list           = &ramfs_dev_list[0];
	gd->bsp_sources.ramfs_dev_num            = ramfs_dev_size;
	gd->bsp_sources.filesystem_dev_list      = &filesystem_dev_list[0];
	gd->bsp_sources.filesystem_dev_num       = filesystem_dev_size;

	gd->boot_setup.video_mode_list           = video_mode_list;
	gd->boot_setup.video_mode_num            = video_mode_size;
#ifdef CONFIG_OF_LIBFDT_OVERLAY
	gd->boot_setup.overlay_peripheral_list   = overlay_peripheral_list;
	gd->boot_setup.overlay_peripheral_num    = overlay_peripheral_size;
#endif
#endif

	strap_get_board_cfg();
}
#endif

void usb3_enable(void)
{
	struct gpio_desc gpio1_01; // USB 3
	struct udevice *gpio_dev1 = NULL;

	uclass_get_device_by_name(UCLASS_GPIO, "gpio1@ff730000" , &gpio_dev1);

	gpio1_01.dev = gpio_dev1;
	gpio1_01.offset = 1;
	gpio1_01.flags = 0;
	dm_gpio_request(&gpio1_01, "USB3_5V_EN");
	dm_gpio_set_dir_flags(&gpio1_01, GPIOD_IS_OUT);

	dm_gpio_set_value(&gpio1_01, 0);
	udelay(1000);

	dm_gpio_set_value(&gpio1_01, 1);

	udelay(1000);
}

int misc_init_r(void)
{
	setup_serial();
	setup_macaddr();
#ifndef CONFIG_SPL_BUILD
#ifdef CONFIG_SECO_ENV_AUTOSET
	autoset_boot_device();
#endif
	setup_seco_environment();
#endif

	usb3_enable();

	return 0;
}

#ifdef CONFIG_SERIAL_TAG
void get_board_serial(struct tag_serialnr *serialnr)
{
	char *serial_string;
	u64 serial = 0;

	serial_string = env_get("serial#");

	if (serial_string)
		serial = simple_strtoull(serial_string, NULL, 16);

	serialnr->high = (u32)(serial >> 32);
	serialnr->low = (u32)(serial & 0xffffffff);
}
#endif

#ifdef CONFIG_USB_DWC3
static struct dwc3_device dwc3_device_data = {
	.maximum_speed = USB_SPEED_HIGH,
	.base = 0xfe800000,
	.dr_mode = USB_DR_MODE_PERIPHERAL,
	.index = 0,
	.dis_u2_susphy_quirk = 1,
	.usb2_phyif_utmi_width = 16,
};

int usb_gadget_handle_interrupts(void)
{
	dwc3_uboot_handle_interrupt(0);
	return 0;
}

int board_usb_init(int index, enum usb_init_type init)
{
	return dwc3_uboot_init(&dwc3_device_data);
}
#endif


#ifndef CONFIG_SPL_BUILD
#if defined(CONFIG_ENV_IS_IN_MMC)
int mmc_get_env_dev(void) {
	int ret;
	char *devtype = NULL, *devnum = NULL;
	ret = param_parse_bootdev(&devtype, &devnum);
	if (!ret) {
        	return atoi(devnum);
	} else {
		return 0;
	}
}
#endif
#endif
