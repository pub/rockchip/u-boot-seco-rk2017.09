/*
 *
 *
 * Reading C61 Board type
 *
 * marco.sandrelli@seco.com
 * tommaso.merciai@seco.com
 *
 *
 */

#include <common.h>
#include <command.h>
#include <malloc.h>
#include <errno.h>
#include <asm/io.h>
#include <miiphy.h>
#include <netdev.h>
#include <asm-generic/gpio.h>
#include <fsl_esdhc.h>
#include <mmc.h>
#include <asm/arch/sys_proto.h>
#include "rk3399_c31_strap_cfg.h"
#include <seco/env_common.h>
#include <adc.h>


#define STR_HELPER(x) #x
#define STR(x) STR_HELPER(x)

DECLARE_GLOBAL_DATA_PTR;

/* Port2 Conf 

	PORTA2

	RS232_2 (D2) 		ADC voltage is value < 500mV
	RS485_2 (D3) 		ADC voltage is 500mV < value < 700mV
	GPIOs(2OUT; 2IN) (D4)	ADC coltage is value > 700mV

*/
#define PORT2_CONF_LIM1_VAL        284 /* 0.5/1.8 *1024 = low limit for conf D2 */
#define PORT2_CONF_LIM2_VAL        398 /* 0.7/1.8 *1024 = high limit for conf D3 */

/*
 * Two board variants whith adc channel 3 is for board id
 * v10: 1024, v11: 512
 * v10: adc channel 0 for dnl key
 * v11: adc channel 1 for dnl key
 */


typedef enum  {
	port1_setup = 0,
	port2_setup,
} seco_config_option_t;

int strap_get_port2_cfg ( void ) {

        unsigned int id_val;

        if (adc_channel_single_shot("saradc", 3, &id_val)) {
                printf("%s read board id failed\n", __func__);
                return false;
        }

        if (id_val < PORT2_CONF_LIM1_VAL) 
                return strap_rs232_cfg;
        else if (id_val > PORT2_CONF_LIM1_VAL && id_val < PORT2_CONF_LIM2_VAL) 
                return strap_rs485_cfg;
	else if (id_val > PORT2_CONF_LIM2_VAL)
		return strap_gpio_cfg;
	
	return -1;
}

void strap_get_board_cfg(void){

	int sel;
	char per_options[256];
	char *port2_sel_dtbo = "";

	/* check port2 strap cfg  and select right dtbo*/
	sel = strap_get_port2_cfg();

	switch(sel)
	{
		case strap_rs232_cfg:
			port2_sel_dtbo = STR("\0");
			break;
		case strap_rs485_cfg:
			port2_sel_dtbo = STR("seco-rk3399-c31-rs485.dtbo");
			break;
		case strap_gpio_cfg:
			port2_sel_dtbo = STR("seco-rk3399-c31-gpio.dtbo");
			break;
	}


	/* append peripheral/video selected dtbo */
	snprintf(per_options, sizeof(per_options), "%s ", port2_sel_dtbo);

	env_set("fdt_overlay_per_dynamic_list", per_options);
}
