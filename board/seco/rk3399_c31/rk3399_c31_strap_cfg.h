/*
 *
 *
 * Reading RK3399 Board type
 *
 * marco.sandrelli@seco.com
 * tommaso.merciai@seco.com
 *
 *
 */


int strap_get_port2_cfg (void);
void strap_get_board_cfg(void);


typedef enum  {
	strap_rs232_cfg = 0,
	strap_rs485_cfg = 1,
	strap_gpio_cfg = 2,
} strap_per_cfg_t;

