/*
 * (C) Copyright 2015 Seco
 *
 * Author: Davide Cardillo <davide.cardillo@seco.com>
 *
 * SPDX-License-Identifier:     GPL-2.0+
 *
 * This code provide a tool to properly configure the boot environment
 * for the Seco Boards.
 *
 */

#include <common.h>
#include <command.h>
#include <environment.h> 
#include <stdlib.h>

#include <seco/env_common.h>
#include "seco_config_lib.h"
#include "seco/env_common.h"


DECLARE_GLOBAL_DATA_PTR;


int hex2int (char ch) {
    if (ch >= '0' && ch <= '9')
        return ch - '0';
    if (ch >= 'A' && ch <= 'F')
        return ch - 'A' + 10;
    if (ch >= 'a' && ch <= 'f')
        return ch - 'a' + 10;
    return -1;
}


int ctoi (char ch) {
	int retval = 0;
	if (ch <= '9' && ch >= '0') {
		retval = ch - '0';
	}
	return retval;
}

char *getline (void) {
	static char buffer[100];
	char c;
	size_t i;

	i = 0;
	while (1) {
		buffer[i] = '\0';
		while (!tstc()){
#ifdef WATCHDOG_RESET
			WATCHDOG_RESET();
#endif
			continue;
		}

		c = getc();
		/* Convert to uppercase */
		//if (c >= 'a' && c <= 'z')
		//	c -= ('a' - 'A');

		switch (c) {
			case '\r':	/* Enter/Return key */
			case '\n':
				puts("\n");
				return buffer;

			case 0x03:	/* ^C - break */
				return NULL;

			case 0x08:	/* ^H  - backspace */
			case 0x7F:	/* DEL - backspace */
				if (i) {
					puts("\b \b");
					i--;
				}
				break;

			default:
				/* Ignore control characters */
				if (c < 0x20)
					break;
				/* Queue up all other characters */
				buffer[i++] = c;
				printf("%c", c);
				break;
		}
	}
}



/*  __________________________________________________________________________
 * |                                                                          |
 * |                            RAM Size Selection                            |
 * |__________________________________________________________________________|
 */
char *do_ramsize (ulong min, ulong max) {
	char *line;
	do {printf ("\n __________________________________________________");
		printf ("\n       Choose Kernel RAM size to allocate\n");
		printf (" __________________________________________________\n");
		printf ("[min size: %luM - max size %luM]\n", min, max);
		printf ("> ");
		line = getline ();
	}while ((ulong)atoi(line) < min || (ulong)atoi(line) > max);
	printf ("Will use %luM of RAM memory\n",(ulong)atoi(line));
	return line;
}



/*  __________________________________________________________________________
 * |                                                                          |
 * |                           Parameters acquiring                           |
 * |__________________________________________________________________________|
 */

int selection_dev (char *scope, data_boot_dev_t dev_list[], int num_element) {
	char ch;
	int i, selection = 0;

	do {
		printf ("\n __________________________________________________");
		printf ("\n            Choose boot Device for %s.\n", scope);
		printf (" __________________________________________________\n");
		for ( i = 0 ; i < num_element ; i++ ) {
			printf ("%d) %s\n", i+1, dev_list[i].label);
		}
		printf ("> ");
		ch = getc ();
		printf ("%c\n", ch);
	} while ((ctoi(ch) < 1) || (ctoi(ch) > num_element));
	
	selection = ctoi(ch) - 1;

	return selection;
}


void select_partition_id (char *partition_id) {
	char ch;

	do {
		printf ("Choose the partition\n");
		printf ("> ");
		ch = getc ();
		printf ("%c\n", ch);
	} while (ctoi(ch) < MIN_PARTITION_ID || ctoi(ch) > MAX_PARTITION_ID);

	sprintf (partition_id, "%c", ch);
}


void select_source_path (char *source_path, char *subject, char *default_path) {
	char *line;

	printf ("Path of the %s (enter for default %s) > ", subject, default_path);
	line = getline ();

	strcpy (source_path, strcmp (line, "") == 0 ? default_path : line);
}



void select_tftp_parameters (int *used_dhcp, char *serverip_tftp , char *ipaddr_tftp) {
	char *line;
	static char str[20];
	char *pstr;
	char ch;
	
	printf ("\n ________________________________________________________\n");
	printf (" Select TFTP as source for kernel, please set the net\n");
	printf (" ________________________________________________________\n");

	/*  Set DHCP configuration  */
	do { 
		printf ("\nDo you want to use dynamic ip assignment (DHCP) for tftp? (y/n)\n");
		printf ("> ");
		ch = getc ();
	} while (ch != 'y' && ch != 'n');
	if (ch == 'y') {
		*used_dhcp = 1;
		printf ("\nYou have select to use dynamic ip\n");
	} else {
		*used_dhcp = 0;
		printf ("\nYou have select to use static ip\n");
	}

	if ( *used_dhcp ) {
		return;
	}

	do {
		pstr = env_get ("serverip");
		if ( pstr == NULL ) {
			strcpy (str, __stringify(CONFIG_SERVERIP));
			pstr = &str[0];
		}
		printf ("\nInsert the address ip of the tftp server (enter for current: %s)\n", 
				pstr);
		printf ("> ");
                line = getline ();
                printf ("%s\n", line);
        } while (0);
	strcpy (serverip_tftp, strcmp (line, "") == 0 ? pstr : line);

	do {
		pstr = env_get ("ipaddr");
		if ( pstr == NULL ) {
			strcpy (str, __stringify(CONFIG_IPADDR));
			pstr = &str[0];
		}
		printf ("\nInsert the address ip of this tftp client (enter for current: %s)\n",
				pstr);
		printf ("> ");
                line = getline ();
                printf ("%s\n", line);
        } while (0);
	strcpy (ipaddr_tftp, strcmp (line, "") == 0 ? pstr : line);
}


void select_nfs_parameters (char *ip_local, char *ip_server, char *nfs_path, char *netmask, int *dhcp_set, int *dhcp_auto) {
	char *line;
	char ch;
	static char str[30];
	char *pstr;


	/*  Set DHCP configuration  */
	do { 
		printf ("\nDo you want to use dynamic ip assignment (DHCP)? ((y)es, (n)o, (a)automatic)\n");
		printf( "whith automatic the remote root path will be not specified.\n");
		printf ("> ");
		ch = getc ();
	} while (ch != 'y' && ch != 'n' && ch != 'a');
	if (ch == 'y') {
		*dhcp_set = 1;
		*dhcp_auto = 0;
		printf ("\nYou have select to use dynamic ip\n");
	} else if ( ch == 'n' ) {
		*dhcp_set = 0;
		*dhcp_auto = 0;
		printf ("\nYou have select to use static ip\n");
	} else {
		*dhcp_set = 1;
		*dhcp_auto = 1;
		printf ("\nYou have select to use dynamic ip with automatic root path\n");
	}

	if ( *dhcp_auto == 0 ) {
		
		/* Set the NFS Path  */
		do {
			pstr = GET_NFS_PATH;
			if ( pstr == NULL ) {
				strcpy (str, DEF_NFS_PATH);
				pstr = &str[0];
			}

			printf ("Insert the nfs path of the host machine (enter for current: %s)\n",
					pstr);
			printf ("> ");
			line = getline ();
			printf ("%s\n", line);
		} while (0);
		strcpy (nfs_path, strcmp (line, "") == 0 ? pstr : line);

	}

	if (*dhcp_set == 0) {

		/*  Set HOST IP  */
		do {
			pstr = GET_NFS_IP_SERVER;
			if ( pstr == NULL ) {
				strcpy (str, __stringify(CONFIG_SERVERIP));
				pstr = &str[0];
			}

			printf ("Insert the address ip of the host machine (enter for current: %s)\n",
					pstr);
			printf ("> ");
			line = getline ();
			printf ("%s\n", line);
		} while (0);
		strcpy (ip_server, strcmp (line, "") == 0 ? pstr : line);
		
		/* Set CLIENT IP  */
		do {
			pstr = GET_NFS_IP_CLIENT;
			if ( pstr == NULL ) {
				strcpy (str, __stringify(CONFIG_IPADDR));
				pstr = &str[0];
			}

			printf ("Insert an address ip for this board (enter for current: %s)\n",
					 pstr);
			printf ("> ");
			line = getline ();
			printf ("%s\n", line);
		} while (0);
		strcpy (ip_local, strcmp (line, "") == 0 ? pstr : line);


		/* set NETMASK of the CLIENT  */
		do {
			pstr = GET_NFS_NETMASK;
			if ( pstr == NULL ) {
				strcpy (str, __stringify(DEF_NETMASK));
				pstr = &str[0];
			}

			printf ("Insert the netmask (enter for current: %s)\n",
					pstr);
			printf ("> ");
			line = getline ();
			printf ("%s\n", line);
		} while (0);
		strcpy (netmask, strcmp (line, "") == 0 ? pstr : line);
	}

}


#ifdef CONFIG_OF_LIBFDT_OVERLAY
/*  __________________________________________________________________________
 * |                                                                          |
 * |                             FDT Overlay Selection                        |
 * |__________________________________________________________________________|
 */
void selection_fdt_overlay (overlay_list_t list[], int num_element, int *selections) {
	int i, j, num_opts;
	char ch;

	for ( i = 0 ; i < num_element ; i++ ) {

		if ( list[i].title == NULL )
			break;

		do {
			printf ("\n __________________________________________________");
			printf ("\n     Choose FDT overlay option for %s.\n", list[i].title);
			printf (" __________________________________________________\n");

			j=0;
			while ( list[i].options[j].label != NULL ) {
				printf( "%d) %s\n", j+1, list[i].options[j].label);
				j++;
			}
			num_opts = j;
			
			printf ("> ");
			ch = getc ();
			printf ("%c\n", ch);
		} while ((ctoi(ch) < 1) || (ctoi(ch) > num_opts));

		*(selections + i) = ctoi(ch) - 1;
	}
}


char *create_fdt_overlay_load_string(data_boot_dev_t *table_fdt, data_boot_dev_t *table_overlay, 
				int n_element, char *file_list, int fdt_selected_device ) {

	int  i = 0;
	int  sel_str_load = 0;
	char *list;
	char *fdt_load;
	char *fdtoverlay2ram;

	list = strtok( file_list, " " );
	if ( list != NULL ) {
		fdtoverlay2ram = malloc( sizeof(char) * 1024 );
		if ( fdtoverlay2ram == NULL )
			return NULL;

		memset( fdtoverlay2ram, 0,  sizeof( fdtoverlay2ram ) );	

		if ( fdt_selected_device == -1 ) {
			fdt_load = GET_FDT_BOOT_LOAD;
			if ( fdt_load != NULL ) {
				for ( i = 0 ; i < n_element ; i++ ) {
					if ( strcmp( &table_fdt[i].env_str[0], fdt_load ) == 0 ) {
						sel_str_load = i;
						break;
					}
				}
			} 
		} else {
			sel_str_load = fdt_selected_device;
		}
	
		while ( list != NULL ) {
			
			sprintf( fdtoverlay2ram, "%s %s 0x%08X %s; %s  0x%08X;",
										fdtoverlay2ram,
										table_overlay[sel_str_load].env_str,
										ENV_FDT_OVERLAY_BASEADDR + ENV_FDT_OVERLAY_BASEADDR_OFFSET * i,
										list,
										__stringify(MACRO_ENV_FDT_OVERLAY_APPLY),
										ENV_FDT_OVERLAY_BASEADDR + ENV_FDT_OVERLAY_BASEADDR_OFFSET * i );

			i++;
			list = strtok( NULL, " " );

		}

	} else {

		return NULL;

	}

	return fdtoverlay2ram;
}
#endif  /* CONFIG_OF_LIBFDT_OVERLAY */


/*  __________________________________________________________________________
 * |                                                                          |
 * |              Kernel/FDT/RAMFS/FileSystem Source Selection                |
 * |__________________________________________________________________________|
 */
int select_kernel_source (data_boot_dev_t *table, int n_element, char *partition_id, char *file_name,
			char *spi_load_addr, char *spi_load_len, int *use_tftp) {

	char *str;
	int dev_selected = selection_dev ("Kernel", table, n_element);

	switch ( table[dev_selected].dev_type ) {
		case DEV_EMMC:
		case DEV_U_SD:
		case DEV_EXT_SD:
		case DEV_SATA:
		case DEV_USB:
			select_partition_id (partition_id);
			break;
		case DEV_SPI:
			//select_spi_parameters (spi_load_addr, spi_load_len);
			break;
		case DEV_TFTP:
			*use_tftp = 1;
			break;
		default:
			break;
	}

	str = GET_KERNEL_PATH;
	if ( str != NULL )
		select_source_path (file_name, "Kernel", str);
	else
		select_source_path (file_name, "Kernel", table[dev_selected].def_path);

	return dev_selected;
}


int select_fdt_source (data_boot_dev_t *table, int n_element, char *partition_id, char *file_name,
			char *spi_load_addr, char *spi_load_len, int *use_tftp) {

	char *str;
	char *autodetect_str;
	int dev_selected = selection_dev ("FDT", table, n_element);

	switch ( table[dev_selected].dev_type ) {
		case DEV_EMMC:
		case DEV_U_SD:
		case DEV_EXT_SD:
		case DEV_SATA:
		case DEV_USB:
			select_partition_id (partition_id);
			break;
		case DEV_SPI:
			//select_spi_parameters (spi_load_addr, spi_load_len);
			break;
		case DEV_TFTP:
			*use_tftp = 1;
			break;
		default:
			break;
	}

	autodetect_str = env_get ("fdt_autodetect");
	if ( (autodetect_str != NULL) && (strcmp(autodetect_str, "yes") == 0) ) {
		printf( "\nFDT file name autodetect: %s\n", env_get ("fdt_file") );
	} else {

		str = GET_FDT_PATH;
		if ( str != NULL )
			select_source_path (file_name, "FDT", str);
		else
			select_source_path (file_name, "FDT", table[dev_selected].def_path);
	}
	return dev_selected;
}


int select_ramfs_source (data_boot_dev_t *table, int n_element, char *partition_id, char *file_name,
                        char *spi_load_addr, char *spi_load_len, int *use_tftp) {

        char *str;
        int dev_selected = selection_dev ("Ramfs", table, n_element);

	    switch ( table[dev_selected].dev_type ) {
				case DEV_NONE:
					return dev_selected;
                case DEV_EMMC:
                case DEV_U_SD:
                case DEV_EXT_SD:
                case DEV_SATA:
                case DEV_USB:
                        select_partition_id (partition_id);
                        break;
                case DEV_SPI:
                        break;
                case DEV_TFTP:
                        *use_tftp = 1;
                        break;
                default:
                        break;
        }

        str = GET_RAMFS_PATH;
        if ( str != NULL )
                select_source_path (file_name, "Ramfs", str);
        else
                select_source_path (file_name, "Ramfs", table[dev_selected].def_path);

        return dev_selected;
}


int select_filesystem_souce (data_boot_dev_t *table, int n_element, char *partition_id, char *nfs_path, 
			char *serverip_nfs , char *ipaddr_nfs, char *netmask_nfs, int *use_dhcp, int *use_auto_dhcp) {

	int dev_selected = selection_dev ("FileSystem", table, n_element);

	switch ( table[dev_selected].dev_type ) {
		case DEV_EMMC:
		case DEV_U_SD:
		case DEV_EXT_SD:
		case DEV_SATA:
		case DEV_USB:
			select_partition_id (partition_id);
			break;
		case DEV_NFS:
			select_nfs_parameters (ipaddr_nfs, serverip_nfs, nfs_path, netmask_nfs, use_dhcp, use_auto_dhcp);
			break;
		default:
			break;
	}

	return dev_selected;
}


/*  __________________________________________________________________________
 * |                                                                          |
 * |                 API Kernel/FDT/RAMFS/FileSystem Setting                  |
 * |__________________________________________________________________________|
 */

int set_kernel_source( int *use_tftp ) {
	char kernel_part_id[2];
	char kernel_filename[100];
	char kernel_spi_load_address[20];
	char kernel_spi_load_length[20];
	int  kernel_selected_device = -1;

	kernel_selected_device = select_kernel_source (gd->bsp_sources.kern_dev_list, gd->bsp_sources.kern_dev_num,
				kernel_part_id,	kernel_filename, kernel_spi_load_address,
				kernel_spi_load_length, use_tftp);

	SAVE_KERNEL_DEVICE_ID(gd->bsp_sources.kern_dev_list[kernel_selected_device].device_id);
	SAVE_KERNEL_LOADADDR(gd->bsp_sources.kern_dev_list[kernel_selected_device].load_address);

	if ( gd->bsp_sources.kern_dev_list[kernel_selected_device].dev_type != DEV_TFTP ) {
		SAVE_KERNEL_PATH(kernel_filename);
	}

	switch (gd->bsp_sources.kern_dev_list[kernel_selected_device].dev_type) {
		case DEV_EMMC:
		case DEV_U_SD:
		case DEV_EXT_SD:
		case DEV_SATA:
		case DEV_USB:
			SAVE_KERNEL_PARTITION(kernel_part_id);
			break;
		case DEV_SPI:
			SAVE_KERNEL_SPI_ADDR(kernel_spi_load_address);
			SAVE_KERNEL_SPI_LEN(kernel_spi_load_length);
			break;
		case DEV_TFTP:
			break;
		default:
			break;
	}

	SAVE_KERNEL_BOOT_LOAD(gd->bsp_sources.kern_dev_list[kernel_selected_device].env_str);

	return kernel_selected_device;
}


int set_fdt_source( int *use_tftp ) {
	int fdt_selected_device = 0;
	char *autodetect_str;
	char fdt_part_id[10];
	char fdt_filename[100];
	char fdt_spi_load_address[20];
	char fdt_spi_load_length[20];

	fdt_selected_device = select_fdt_source (gd->bsp_sources.fdt_dev_list, gd->bsp_sources.fdt_dev_num, 
				fdt_part_id, fdt_filename, fdt_spi_load_address,
				fdt_spi_load_length, use_tftp);

	SAVE_FDT_DEVICE_ID(gd->bsp_sources.fdt_dev_list[fdt_selected_device].device_id);
	SAVE_FDT_LOADADDR(gd->bsp_sources.fdt_dev_list[fdt_selected_device].load_address);

	if ( gd->bsp_sources.fdt_dev_list[fdt_selected_device].dev_type != DEV_TFTP ) {
		autodetect_str = env_get ("fdt_autodetect");
		if ( (autodetect_str == NULL) || (strcmp(autodetect_str, "no") == 0) ) {
			SAVE_FDT_PATH(fdt_filename);
		}
	}

	switch (gd->bsp_sources.fdt_dev_list[fdt_selected_device].dev_type) {
		case DEV_EMMC:
		case DEV_U_SD:
		case DEV_EXT_SD:
		case DEV_SATA:
		case DEV_USB:
			SAVE_FDT_PARTITION(fdt_part_id);
			break;
		case DEV_SPI:
			SAVE_FDT_SPI_ADDR(fdt_spi_load_address);
			SAVE_FDT_SPI_LEN(fdt_spi_load_length);
			break;
		case DEV_TFTP:
			break;
		default:
			break;
	}

	SAVE_FDT_BOOT_LOAD(gd->bsp_sources.fdt_dev_list[fdt_selected_device].env_str);

	return 0;
}


int set_ramfs_source( int *use_tftp ) {
	int ramfs_selected_device = -1;
	int use_ramfs = 0;
	char ramfs_part_id[2];
    char ramfs_filename[100];
    char ramfs_spi_load_address[20];
    char ramfs_spi_load_length[20];

	ramfs_selected_device = select_ramfs_source (gd->bsp_sources.ramfs_dev_list, gd->bsp_sources.ramfs_dev_num, 
				ramfs_part_id, ramfs_filename, ramfs_spi_load_address,
				ramfs_spi_load_length, use_tftp);

	use_ramfs = 0;
	switch (gd->bsp_sources.ramfs_dev_list[ramfs_selected_device].dev_type) {
		case DEV_EMMC:
		case DEV_U_SD:
		case DEV_EXT_SD:
		case DEV_SATA:
		case DEV_USB:
			use_ramfs = 1;
			SAVE_RAMFS_PARTITION(ramfs_part_id);
			break;
		case DEV_SPI:
			use_ramfs = 1;
			SAVE_RAMFS_SPI_ADDR(ramfs_spi_load_address);
			SAVE_RAMFS_SPI_LEN(ramfs_spi_load_length);
			break;
		case DEV_TFTP:
			use_ramfs = 1;
			break;
		default:
			break;
	}

	if ( use_ramfs ) {
		SAVE_RAMFS_DEVICE_ID(gd->bsp_sources.ramfs_dev_list[ramfs_selected_device].device_id);
		SAVE_RAMFS_LOADADDR(gd->bsp_sources.ramfs_dev_list[ramfs_selected_device].load_address);

		if ( gd->bsp_sources.ramfs_dev_list[ramfs_selected_device].dev_type != DEV_TFTP ) {
			SAVE_RAMFS_PATH(ramfs_filename);
		}

		SAVE_RAMFS_BOOT_LOAD(gd->bsp_sources.ramfs_dev_list[ramfs_selected_device].env_str);

		env_set ("ramfs_use", "1");
	} else {
		env_set ("ramfs_use", "0");
	}

	return ramfs_selected_device;
} 


void set_for_tftp( int use_tftp ) {
	char serverip_tftp[50];
	char ipaddr_tftp[50];
	int use_dhcp;

	if ( use_tftp ) {
		select_tftp_parameters (&use_dhcp, serverip_tftp , ipaddr_tftp);
		if ( use_dhcp ) {
			env_set ("tftp_use_dhcp", "1");
		} else {
			env_set ("run_from_tftp", "1");
			env_set ("ipsetup", "setenv ipaddr ${ipaddr}; setenv serverip ${serverip};");
			env_set ("serverip", serverip_tftp);
			env_set ("ipaddr", ipaddr_tftp);
			env_set ("tftp_use_dhcp", "0");
		}
	} else {
		env_set ("run_from_tftp", "0");
		env_set ("tftp_use_dhcp", "0");
	}
}


int set_filesystem_source( void ) {
	int filesystem_selected_device;
	char filesystem_part_id[2];
	char filesystem_server_nfs[50];
	char filesystem_ipaddr_nfs[50];
	char filesystem_netmask_nfs[50];
	int  filesystem_use_dhcp, filesyste_use_auto_dhcp;
	char filesystem_path_nfs[300];
	char filesystem_boot_string[500];

	filesystem_selected_device = select_filesystem_souce (gd->bsp_sources.filesystem_dev_list, gd->bsp_sources.filesystem_dev_num, 
			filesystem_part_id, filesystem_path_nfs, filesystem_server_nfs, filesystem_ipaddr_nfs,
				filesystem_netmask_nfs, &filesystem_use_dhcp, &filesyste_use_auto_dhcp);

	switch (gd->bsp_sources.filesystem_dev_list[filesystem_selected_device].dev_type) {
		case DEV_EMMC:
		case DEV_U_SD:
		case DEV_EXT_SD:
			SAVE_FILESYSTEM_DEVICE_ID(gd->bsp_sources.filesystem_dev_list[filesystem_selected_device].device_id);
			SAVE_FILESYSTEM_PARTITION(filesystem_part_id);
			break;
		case DEV_SATA:
		case DEV_USB:
			SAVE_FILESYSTEM_PARTITION(filesystem_part_id);
			break;
		case DEV_SPI:
			break;
		case DEV_NFS:
			SAVE_NFS_USE("1");
			if ( filesystem_use_dhcp == 1 ) {
				SAVE_NFS_USE_DHCP("1");
				if ( filesyste_use_auto_dhcp ) {
					SAVE_NFS_USE_DHCP_AUTO("1");
				} else {
					SAVE_NFS_USE_DHCP_AUTO("0");
					SAVE_NFS_PATH(filesystem_path_nfs);
				}
			} else {
				SAVE_NFS_USE_DHCP("0");
				SAVE_NFS_USE_DHCP_AUTO("0");
				SAVE_NFS_IP_CLIENT(filesystem_ipaddr_nfs);
				SAVE_NFS_IP_SERVER(filesystem_server_nfs);
				SAVE_NFS_NETMASK(filesystem_netmask_nfs);
				SAVE_NFS_PATH(filesystem_path_nfs);
			}
			break;
		case DEV_TFTP:
			break;
		default:
			break;
	}

	if ( gd->bsp_sources.filesystem_dev_list[filesystem_selected_device].dev_type != DEV_NFS )
		SAVE_NFS_USE("0");

	sprintf (filesystem_boot_string, "env_set root_dev \'%s\'",
			gd->bsp_sources.filesystem_dev_list[filesystem_selected_device].env_str);

	SAVE_FILESYSTEM_ROOT(gd->bsp_sources.filesystem_dev_list[filesystem_selected_device].env_str);

	return filesystem_selected_device;
}



/*  __________________________________________________________________________
 * |                                                                          |
 * |                         Video Settings Selection                         |
 * |__________________________________________________________________________|
 */
int selection_video_mode (video_mode_t  video_mode_list[], int num_element) {
	char ch;
	int i, selection = 0;

	do {
		printf ("\n __________________________________________________");
		printf ("\n               Choose Video Setting.\n");
		printf (" __________________________________________________\n");
		for ( i = 0 ; i < num_element ; i++ ) {
			printf ("%d) %s\n", i+1, video_mode_list[i].label);
		}
		printf ("> ");
		ch = getc ();
		printf ("%c\n", ch);
	} while ((ctoi(ch) < 1) || (ctoi(ch) > num_element));

	selection = ctoi(ch) - 1;

	return selection;
}


#define ALWAYS_SAVE_FDT_OVERLAY_CMD 1 

int set_video_mode( int fdt_selected_device ) {
	int  video_mode_selection;
#ifdef CONFIG_OF_LIBFDT_OVERLAY
	/*  for Video FDT overlay  */
	char video_overlay_list[256];
	char *fdtoverlay_video_load;
#endif  /* CONFIG_OF_LIBFDT_OVERLAY */

	/* select video mode configuration */
	video_mode_selection = selection_video_mode( gd->boot_setup.video_mode_list,
				gd->boot_setup.video_mode_num );

	/* video setting for u-boot slash screen */
	if ( gd->boot_setup.video_mode_list[video_mode_selection].panel_name != NULL ) {
		SET_VIDEO_PANEL( gd->boot_setup.video_mode_list[video_mode_selection].panel_name );
	} 

	#ifdef CONFIG_OF_LIBFDT_OVERLAY
		/* video setting for a fdt overlay file */
		memset( video_overlay_list, 0,  sizeof( video_overlay_list ) );
		if ( gd->boot_setup.video_mode_list[video_mode_selection].dtbo_conf_file != NULL ) {
			if ( strcmp( gd->boot_setup.video_mode_list[video_mode_selection].dtbo_conf_file, "" ) != 0 ) {
				
				sprintf( video_overlay_list, "%s", gd->boot_setup.video_mode_list[video_mode_selection].dtbo_conf_file );
				SAVE_FDT_OVERLAY_VIDEO_LIST( video_overlay_list );

#if ALWAYS_SAVE_FDT_OVERLAY_CMD
				fdtoverlay_video_load = create_fdt_overlay_load_string(
					gd->bsp_sources.fdt_dev_list, gd->bsp_sources.fdt_overlay_dev_list,
					gd->bsp_sources.fdt_dev_num, video_overlay_list, fdt_selected_device );
				SAVE_FDT_OVERLAY_VIDEO_CMD( fdtoverlay_video_load );
#endif
			}
		} else {
			SAVE_FDT_OVERLAY_VIDEO_LIST( "" );
			SAVE_FDT_OVERLAY_VIDEO_CMD( "" );
		}
#endif  /* CONFIG_OF_LIBFDT_OVERLAY */

	return video_mode_selection;
}

int is_seco_config_sel_used (char *str) {
	int sel;

	if(strcmp(str, SECO_CONFIG_OVERLAY_SEL_ENABLED) == 0) {
		/* static */
		sel = USE_SECO_CONFIG_SEL;
	} else {
		/* dynamic */
		sel = USE_BOARD_STRAP_SEL;
	}

	return sel;
}

inline int is_seco_config_per_used(void) {
	return is_seco_config_sel_used(GET_FDT_OVERLAY_PER_SEL_LOGIC);
}

inline int is_seco_config_video_used(void) {
	return is_seco_config_sel_used(GET_FDT_OVERLAY_VIDEO_SEL_LOGIC);
}

void unset_overlay_per_var( void ) {

	SAVE_FDT_OVERLAY_PER_LIST("");
	SAVE_FDT_OVERLAY_PER_CMD("");
}

void unset_overlay_per_dyn_var( void ) {

	SAVE_FDT_OVERLAY_PER_DYN_LIST("");
	SAVE_FDT_OVERLAY_PER_DYN_CMD("");
}

void unset_overlay_video_var( void ) {

	SAVE_FDT_OVERLAY_VIDEO_LIST("");
	SAVE_FDT_OVERLAY_VIDEO_CMD("");
}

void unset_overlay_video_dyn_var( void ) {

	SAVE_FDT_OVERLAY_VIDEO_DYN_LIST("");
	SAVE_FDT_OVERLAY_VIDEO_DYN_CMD("");
}

#ifdef CONFIG_OF_LIBFDT_OVERLAY
void create_overlay_video_load_cmd( void ) {
	char *video_list_overlay;
	char *fdtoverlay_video_load;

	unset_overlay_video_dyn_var();

	video_list_overlay = GET_FDT_OVERLAY_VIDEO_LIST;
	if ( video_list_overlay != NULL ) {
		fdtoverlay_video_load = create_fdt_overlay_load_string( 
					gd->bsp_sources.fdt_dev_list, gd->bsp_sources.fdt_overlay_dev_list,
					gd->bsp_sources.fdt_dev_num, video_list_overlay, -1 );
		SAVE_FDT_OVERLAY_VIDEO_CMD( fdtoverlay_video_load );
	} 	
 }

void create_overlay_video_dynamic_load_cmd( void ) {
	char *video_list_overlay;
	char *fdtoverlay_video_load;

	unset_overlay_video_var();

	video_list_overlay = GET_FDT_OVERLAY_VIDEO_DYN_LIST;
	if ( video_list_overlay != NULL ) {
		fdtoverlay_video_load = create_fdt_overlay_load_string(
					gd->bsp_sources.fdt_dev_list, gd->bsp_sources.fdt_overlay_dev_list,
					gd->bsp_sources.fdt_dev_num, video_list_overlay, -1 );
		SAVE_FDT_OVERLAY_VIDEO_DYN_CMD( fdtoverlay_video_load );
	}
 }
 #endif  /* CONFIG_OF_LIBFDT_OVERLAY */



int selection_video_spec( video_boot_args_t *video_args ) {
	char ch;
	int i, selection = 0;
	int num_element;

	if ( video_args == NULL )
		return -1;

	num_element = video_args->panel_list_size;
	do {
		printf ("\n __________________________________________________");
		printf ("\n        Choose resolution for %s.\n", video_args->name);
		printf (" __________________________________________________\n");
		for ( i = 0 ; i < num_element ; i++ ) {
			printf ("%d) %s\n", i+1, video_args->panel_params[i].label);
		}
		printf ("> ");
		ch = getc ();
		printf ("%c\n", ch);
	} while ((ctoi(ch) < 1) || (ctoi(ch) > num_element));

	selection = ctoi(ch) - 1;

	return selection;
}


int create_video_args( panel_parameters_t *param, char *buffer, char *driver, char *video_args ) {

	if ( param == NULL )
		return 1;

	if ( ( buffer == NULL ) || (driver == NULL ) )
		return -1;

	sprintf( video_args, "video=%s:dev=%s", buffer, driver );

	if ( param->name ) {
		sprintf( video_args, "%s,%s", video_args, param->name );
		/* video setting for u-boot slash screen */
		SET_VIDEO_PANEL( param->name );
	} 

	if ( param->datamap )
		sprintf( video_args, "%s,%s", video_args, param->datamap );

	if ( param->if_map )
		sprintf( video_args, "%s,if=%s", video_args, param->if_map );

	if ( param->bpp != -1 )
		sprintf( video_args, "%s,bpp=%d", video_args, param->bpp );
	
	if ( param->opt )
		sprintf( video_args, "%s %s", video_args, param->opt );

	return 0;
}


int set_video_specification( int video_mode_selection ) {
	int          i = 0, used = 0;
	int          selection_lvds_spec = -1;
	char         videomode[255], tmp[100];
	video_mode_t *sel_video = &gd->boot_setup.video_mode_list[video_mode_selection];

	/* seco_config video is used --> turn video_use_seco_config to 1 */
	SAVE_FDT_OVERLAY_VIDEO_SEL_LOGIC(SECO_CONFIG_OVERLAY_SEL_ENABLED);

	for ( i = 0 ; i < ENV_NUM_VIDEO_OUTPUT ; i++ ) {
		
		if ( sel_video->video[i].used == VIDEO_NOT_USED )
			continue;

		if ( IS_VIDEO_ARGS_NOTUSED(sel_video->video[i].video_args) )
			continue;

		if ( sel_video->video[i].video_args.panel_list_size == 1 ) {
			selection_lvds_spec = 0;
		} else {
			selection_lvds_spec = selection_video_spec( &sel_video->video[i].video_args );
		}

		create_video_args( &sel_video->video[i].video_args.panel_params[selection_lvds_spec], 
								sel_video->video[i].video_args.buffer,
								sel_video->video[i].video_args.driver,
								&tmp[0] );

		if ( used++ )
			sprintf( videomode, "%s %s", videomode, tmp );
		else
			sprintf( videomode, "%s", tmp );
	}

	SET_VIDEOMODE(videomode);

	return 0;
}

/*  __________________________________________________________________________
 * |                                                                          |
 * |                      Peripheral Settings Selection                       |
 * |__________________________________________________________________________|
 */
 #ifdef CONFIG_OF_LIBFDT_OVERLAY

 int set_peripherals( int fdt_selected_device ) {
	int  i;
	int  *peripheral_overlay_selections;
	char peripheral_overlay_list[256];
	char *fdtoverlay_peripheral_load;
	// char *peripheral_list_overlay;

	/* seco_config peripheral is used --> turn per_use_seco_config to 1 */
	SAVE_FDT_OVERLAY_PER_SEL_LOGIC(SECO_CONFIG_OVERLAY_SEL_ENABLED);

	peripheral_overlay_selections = malloc( sizeof(int) *  gd->boot_setup.overlay_peripheral_num );
	memset( peripheral_overlay_selections, 0,  gd->boot_setup.overlay_peripheral_num );
	memset( peripheral_overlay_list, 0,  sizeof( peripheral_overlay_list ) );

	selection_fdt_overlay( gd->boot_setup.overlay_peripheral_list, gd->boot_setup.overlay_peripheral_num,
						 peripheral_overlay_selections );
		
		
	for ( i = 0 ; i < gd->boot_setup.overlay_peripheral_num ; i++ ) {

			if ( gd->boot_setup.overlay_peripheral_list[i].options[peripheral_overlay_selections[i]].dtb_overlay == NULL )
				break;

			if ( i == 0 )
				sprintf( peripheral_overlay_list, "%s", 
					gd->boot_setup.overlay_peripheral_list[i].options[peripheral_overlay_selections[i]].dtb_overlay );
			else
				sprintf( peripheral_overlay_list, "%s %s",
					peripheral_overlay_list,
					gd->boot_setup.overlay_peripheral_list[i].options[peripheral_overlay_selections[i]].dtb_overlay );

			SAVE_FDT_OVERLAY_PER_LIST( peripheral_overlay_list );			

		}

#if ALWAYS_SAVE_FDT_OVERLAY_CMD
		fdtoverlay_peripheral_load = create_fdt_overlay_load_string( 
					gd->bsp_sources.fdt_dev_list, gd->bsp_sources.fdt_overlay_dev_list,
					gd->bsp_sources.fdt_dev_num, peripheral_overlay_list, fdt_selected_device );
		SAVE_FDT_OVERLAY_PER_CMD( fdtoverlay_peripheral_load );
#endif

	return 0;
 }

void create_overlay_per_load_cmd( void ) {
	char *peripheral_list_overlay;
	char *fdtoverlay_peripheral_load;

	/* Unset dynamic variables */
	unset_overlay_per_dyn_var();

	peripheral_list_overlay = GET_FDT_OVERLAY_PER_LIST;
	if ( peripheral_list_overlay != NULL ) {
		fdtoverlay_peripheral_load = create_fdt_overlay_load_string(
					gd->bsp_sources.fdt_dev_list, gd->bsp_sources.fdt_overlay_dev_list,
					gd->bsp_sources.fdt_dev_num, peripheral_list_overlay, -1 );
		SAVE_FDT_OVERLAY_PER_CMD( fdtoverlay_peripheral_load );
	}
}

void create_overlay_per_dynamic_load_cmd( void ) {
	char *peripheral_list_overlay;
	char *fdtoverlay_peripheral_load;

	/* Unset static variables */
	unset_overlay_per_var();

	peripheral_list_overlay = GET_FDT_OVERLAY_PER_DYN_LIST;
	if ( peripheral_list_overlay != NULL ) {
		fdtoverlay_peripheral_load = create_fdt_overlay_load_string( 
					gd->bsp_sources.fdt_dev_list, gd->bsp_sources.fdt_overlay_dev_list,
					gd->bsp_sources.fdt_dev_num, peripheral_list_overlay, -1 );
		SAVE_FDT_OVERLAY_PER_DYN_CMD( fdtoverlay_peripheral_load );
	} 	
}

 #endif  /* CONFIG_OF_LIBFDT_OVERLAY */
