#ifndef __SECO_CONFIG_LIB__
#define __SECO_CONFIG_LIB__

/*  __________________________________________________________________________
 * |___________________________ DEFINE ______________________________|
*/
#define USE_SECO_CONFIG_SEL 1
#define USE_BOARD_STRAP_SEL 0

#define SECO_CONFIG_OVERLAY_SEL_ENABLED  "1"
#define SECO_CONFIG_OVERLAY_SEL_DISABLED "0"

/*  __________________________________________________________________________
 * |___________________________ BASIC FUNCTIONS ______________________________|
*/
char *do_ramsize (ulong min, ulong max);
int selection_dev (char *scope, data_boot_dev_t dev_list[], int num_element);
void select_partition_id (char *partition_id);
void select_source_path (char *source_path, char *subject, char *default_path);
void select_tftp_parameters (int *used_dhcp, char *serverip_tftp , char *ipaddr_tftp);
void select_nfs_parameters (char *ip_local, char *ip_server, char *nfs_path, char *netmask, int *dhcp_set, int *dhcp_auto);

/*  __________________________________________________________________________
 * |_________________________ ADVANCED FUNCTIONS _____________________________|
*/
int select_kernel_source (data_boot_dev_t *table, int n_element, char *partition_id, char *file_name,
			char *spi_load_addr, char *spi_load_len, int *use_tftp);
int select_fdt_source (data_boot_dev_t *table, int n_element, char *partition_id, char *file_name,
			char *spi_load_addr, char *spi_load_len, int *use_tftp);
int select_ramfs_source (data_boot_dev_t *table, int n_element, char *partition_id, char *file_name,
                        char *spi_load_addr, char *spi_load_len, int *use_tftp) ;
int select_filesystem_souce (data_boot_dev_t *table, int n_element, char *partition_id, char *nfs_path, 
			char *serverip_nfs , char *ipaddr_nfs, char *netmask_nfs, int *use_dhcp, int *dhcp_auto);

int selection_video_mode (video_mode_t  video_mode_list[], int num_element);

/*  __________________________________________________________________________
 * |____________________________ API FUNCTIONS _______________________________|
*/
extern int set_kernel_source( int *use_tftp );
extern int set_fdt_source( int *use_tftp );
extern int set_ramfs_source( int *use_tftp );
extern void set_for_tftp( int use_tftp );
extern int set_filesystem_source( void );

extern int set_video_mode( int fdt_selected_device );
extern int selection_video_spec( video_boot_args_t *video_args );
extern int set_video_specification( int video_mode_selection );

extern int set_peripherals( int fdt_selected_device );

extern void create_overlay_video_load_cmd( void );
extern void create_overlay_per_load_cmd( void );

extern void create_overlay_video_dynamic_load_cmd( void );
extern void create_overlay_per_dynamic_load_cmd( void );

int is_seco_config_video_used(void);
int is_seco_config_per_used(void);

#endif     /*  __SECO_CONFIG_LIB__  */
