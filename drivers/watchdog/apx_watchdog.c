/*
 * apx_watchdog.c - driver for i.MX6 a66 Apx Watchdog
 *
 * Licensed under the GPL-2 or later.
 */

#include <common.h>
#include <asm/io.h>
#include <watchdog.h>
#include <asm/gpio.h>
#include <wdt.h>

#ifdef CONFIG_APX_WATCHDOG

#define WDT_IOMUX_REG(x)        ((x))
#define WDT_PADCTRL_REG(x)      ((x))
#define WDT_GDIR_REG(x)         ((x) + 0x4) 
#define WDT_DR_REG(x)           ((x))


 
#define WDT_PAD_MUX_GPIO(x)     writel(readl(WDT_IOMUX_REG((x))) | 0x5, WDT_IOMUX_REG((x)))

#define WDT_PAD_CTRL_GPIO(x)    writel(0x06028, WDT_PADCTRL_REG((x)))

#define WDT_DIR_OUT_GPIO(x,n)   writel(readl(WDT_GDIR_REG((x))) | ( 1 << (n)), WDT_GDIR_REG((x)))

#define WDT_SET_H_GPIO(x,n)     writel(readl(WDT_DR_REG((x))) | (1 << (n)) , WDT_DR_REG((x)))

#define WDT_SET_L_GPIO(x,n)     writel(readl(WDT_DR_REG((x))) & ~(1 << (n)) , WDT_DR_REG((x)))


void hw_watchdog_reset(void)
{
	gpio_request(APX_WDT_TRIGGER, "wdt_trg");
        gpio_request(APX_WDT_ENABLE, "wdt_en");

        gpio_direction_output(APX_WDT_TRIGGER, 0);
        gpio_direction_output(APX_WDT_ENABLE, 1);
#ifdef APX_WDT_EN_LOW
	gpio_set_value(APX_WDT_ENABLE, 0);
#endif
	//APX Watchdog - Refresh watchdog 
	gpio_set_value(APX_WDT_TRIGGER, 1);
	gpio_set_value(APX_WDT_TRIGGER, 0);

}

void hw_watchdog_init(void)
{

	gpio_request(APX_WDT_TRIGGER, "wdt_trg");
	gpio_request(APX_WDT_ENABLE, "wdt_en");

        gpio_direction_output(APX_WDT_TRIGGER, 0);	
	gpio_direction_output(APX_WDT_ENABLE, 1);
#ifdef APX_WDT_EN_LOW
	gpio_set_value(APX_WDT_ENABLE, 0);
#endif
	gpio_set_value(APX_WDT_TRIGGER, 0);
}
#endif

