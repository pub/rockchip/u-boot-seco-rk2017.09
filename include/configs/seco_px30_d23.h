/*
 * (C) Copyright 2020-2021 Seco S.p.A.
 *
 * SPDX-License-Identifier:     GPL-2.0+
 */

#ifndef __SECO_D23_PX30_H
#define __SECO_D23_PX30_H

#include <configs/seco_px30_common.h>


#undef CONFIG_CONSOLE_SCROLL_LINES
#define CONFIG_CONSOLE_SCROLL_LINES		10
#define CONFIG_SUPPORT_EMMC_RPMB


#ifndef CONFIG_SPL_BUILD
#undef CONFIG_BOOTCOMMAND
#define CONFIG_BOOTCOMMAND RKIMG_BOOTCOMMAND
#endif

#define CONFIG_MMC_SDHCI_SDMA
#define CONFIG_SYS_MMC_ENV_DEV 0

#define ENV_ROOT_DEV_ID	                1
#define ENV_ROOT_PARTITION              2

#define SDRAM_BANK_SIZE			(2UL << 30)
#define CONFIG_MISC_INIT_R
#define CONFIG_SERIAL_TAG
#define CONFIG_ENV_OVERWRITE

#define CONFIG_BMP_16BPP
#define CONFIG_BMP_24BPP
#define CONFIG_BMP_32BPP

/* SECOCONFIG VARIABLEs */

/* eMMC uSD Configuration */
#define BOOT_ID_EMMC 0
#define ROOT_ID_EMMC 0
#define BOOT_ID_USD  1
#define ROOT_ID_USD  1
#define BOOT_ID_EXT_SD  2
#define ROOT_ID_EXT_SD  2
#define SECO_NUM_BOOT_DEV                        3



#define ENV_MMCAUTODETECT                        "yes"
#define ENV_FDTAUTODETECT                        "yes"
#define ENV_MEMAUTODETECT                        "yes"

/*  boot file  */
#define ENV_SYS_MMC_ENV_DEV                      0
#define ENV_SYS_MMC_KERNEL_PART                  3
#define ENV_SYS_MMC_FDT_PART                     3
#define ENV_SYS_MMC_RAMFS_PART                   3
/*  boot additional file  */
#define ENV_SYS_MMC_BOOSCRIPT_PART               3
#define ENV_SYS_MMC_BOOATENV_PART                4
/*  rootfs file  */
#define ENV_SYS_MMC_ROOTFS_PART                  4
/*  video outputs */
#define ENV_NUM_VIDEO_OUTPUT                     0   /* lvds, edp, hdmi, typeC */



#define ENV_VIDEO_DEFAULT                        "fdt_overlay_video_cmd=load mmc ${fdt_device_id}:${fdt_partition} 0x08100000 seco-px30-d23-hdmi.dtbo; fdt apply  0x08100000;\0"
#define ENV_LOADADDR                             CONFIG_LOADADDR
#define ENV_DEFAULT_FDT_FILE                     "seco-px30-d23.dtb"
#define ENV_UENV_FILE_NAME                       uEnvD23.txt

#include "seco_environment.h"

#endif
