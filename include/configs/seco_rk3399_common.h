/*
 * (C) Copyright 2015 Seco
 *
 * Author: Davide Cardillo <davide.cardillo@seco.com>
 *
 * SPDX-License-Identifier:     GPL-2.0+
 */


#ifndef __ROCKCHIPSECO_COMMON_CONFIG_H
#define __ROCKCHIPSECO_COMMON_CONFIG_H


/* ____________________________________________________________________________
  |                                                                            |
  |                                   ENVIRONMENT                              |
  |____________________________________________________________________________|
*/

#define CONFIG_SYS_MMC_IMG_LOAD_PART   1

/*
#define FDT_LOADADDR	 	0x08300000
#define OVERLAY_LOADADDR_BASE	0x79000000
#define OVERLAY_LOADADDR_OFFSET 0x00200000
#define HDPRX_LOADADDR		0x88800000
#define HDPTX_LOADADDR		0x88000000


*/


#define CONFIG_SYS_MMC_ENV_PART            0

#define ENV_KERNEL_LOADADDR	               0x00280000
#define ENV_KERNEL_FILENAME                "Image"
#define ENV_FDT_LOADADDR                   0x08300000
#define ENV_FDT_OVERLAY_BASEADDR           0x07900000
#define ENV_FDT_OVERLAY_BASEADDR_OFFSET    0x00020000
#define ENV_FDT_RESIZE                        0xc0000
#define ENV_RAMFS_LOADADDR                 0x0a200000
#define ENV_BOOTSCRIPT_LOADADDR            0x0a100000

#define DEFAULT_OVERLAY_FILE               ""

#define ENV_BOOT_TYPE                      booti

#define ROCKCHIP_DEVICE_SETTINGS \
		"stdout=serial,vidconsole\0" \
		"stderr=serial,vidconsole\0"


#define ENV_CONSOLE_DEV                ROCKCHIP_DEVICE_SETTINGS
#endif 
