#ifndef __SECO_ENV_GD__
#define __SECO_ENV_GD__



/*  __________________________________________________________________________
 * |                                                                          |
 * |                               BOOT PARAMETERS                            |
 * |__________________________________________________________________________|
*/
typedef enum {
	DEV_NONE,
	DEV_EMMC,
	DEV_U_SD,
	DEV_EXT_SD,
	DEV_NAND,
	DEV_SPI,
	DEV_SATA,
	DEV_USB,
	DEV_TFTP,
	DEV_NFS,
} device_t;


typedef struct data_boot_dev {
	device_t         dev_type;
	char             *label;
	char             *env_str;
	char             *device_id;
    char             *load_address;
	char             *def_path;
} data_boot_dev_t;


#ifdef CONFIG_OF_LIBFDT_OVERLAY
typedef struct overlay_struct_mode {
    char *label;
    char *dtb_overlay;
} overlay_struct_mode_t;


typedef struct overlay_list {
	char *title;
	overlay_struct_mode_t options[10];
} overlay_list_t;
#endif  /* CONFIG_OF_LIBFDT_OVERLAY */


typedef struct source_bsp {
/* data sources */
    data_boot_dev_t *kern_dev_list;
    unsigned int    kern_dev_num;
    data_boot_dev_t *fdt_dev_list;
    unsigned int    fdt_dev_num;
	data_boot_dev_t *fdt_overlay_dev_list;
    unsigned int    fdt_overlay_dev_num;
    data_boot_dev_t *ramfs_dev_list;
    unsigned int    ramfs_dev_num;
    data_boot_dev_t *filesystem_dev_list;
    unsigned int    filesystem_dev_num;
} source_bsp_t;



/*  __________________________________________________________________________
 * |                                                                          |
 * |                               VIDEO PARAMETERS                           |
 * |__________________________________________________________________________|
*/
typedef enum {
	NO_VIDEO,
	VIDEO_HDMI,
	VIDEO_LVDS,
	VIDEO_EDP,
	VIDEO_DSI,
	VIDEO_TYPEC
} video_type_t;


typedef struct panel_parameters {
	char     *label;
	char     *name;
	char     *if_map;
	char     *datamap;
	int      bpp;
	char     *opt; 
	int      num_channel;
} panel_parameters_t;


#define VIDEO_NOT_USED       -1
#define VIDEO_USED           1

typedef struct video_boot_args {
	char                *name;
	char                *buffer;
	char                *driver;
	panel_parameters_t  *panel_params;	
	int                 panel_list_size;
} video_boot_args_t;


#define NO_VIDEO_ARGS  { NULL, NULL, NULL, NULL, 0 }

#define IS_VIDEO_ARGS_NOTUSED(x) \
	( ( (x).buffer == NULL ) || ( (x).driver == NULL ) || ( (x).panel_params == NULL ) )


#define PANEL_LIST(x)  .panel_params = (x), \
						.panel_list_size = ARRAY_SIZE((x)),


typedef struct video_hw {
	int                used;
	video_type_t       type;
	video_boot_args_t  video_args;
} video_hw_t;


typedef struct video_mode {
	char        *label;
	video_hw_t  video[ENV_NUM_VIDEO_OUTPUT];
	char        *panel_name;
	int         use_bootargs;
	char        *dtbo_conf_file;
} video_mode_t;


typedef struct boot_setup {
 	/* video output */
	video_mode_t    *video_mode_list;
	int             video_mode_num;
#ifdef CONFIG_OF_LIBFDT_OVERLAY
	data_boot_dev_t *fdt_overlay_dev_list;
    unsigned int    fdt_overlay_dev_num;
	overlay_list_t  *overlay_peripheral_list;
	unsigned int    overlay_peripheral_num;
#endif

} boot_setup_t;
#endif     /*  __SECO_ENV_GD__  */
