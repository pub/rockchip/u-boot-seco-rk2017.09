#define _GNU_SOURCE
#include <fcntl.h>
#include <getopt.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/file.h>
#include <unistd.h>
#include <version.h>


#include <system_data.h>
#include "fw_sysdata.h"

#define likely(x)       __builtin_expect((x),1)
#define unlikely(x)     __builtin_expect((x),0)


/*  _____________________________________________________________________________
 * |                                                                             |
 * |                               ARGUMENT MANAGEMENT                           |
 * |_____________________________________________________________________________|
 */
typedef enum cmd_target {
    CMD_NONE = 0,
    CMD_SWITCHPART,
} cmd_target_t;

static char p_name[254];

typedef enum cmd_switchpart {
    CMD_SWITCHPART_NONE = 0,
    CMD_SWITCHPART_INIT,
    CMD_SWITCHPART_BOOTOK,
    CMD_SWITCHPART_SWITCH,
    CMD_SWITCHPART_COUNTER_EN,
    CMD_SWITCHPART_COUNTER_DIS,
    CMD_SWITCHPART_PRINT,
} cmd_switchpart_t;


static cmd_target_t _cmd = CMD_NONE;
static cmd_switchpart_t _cmd_swichpart = CMD_SWITCHPART_NONE;


static struct option common_options[] = {
	{"target", required_argument, NULL, 't'},
	{"help", no_argument, NULL, 'h'},
	{"version", no_argument, NULL, 'v'},
	{NULL, 0, NULL, 0}
};

static struct option switchpart_options[] = {
	{"initialize", no_argument, NULL, 'i'},
	{"bootok", no_argument, NULL, 'b'},
	{"switch", no_argument, NULL, 's'},
    {"counter", required_argument, NULL, 'c' },
    {"print", no_argument, NULL, 'p'},
	{NULL, 0, NULL, 0}
};

#define ARRAY_SIZE(x)  (sizeof((x)) / sizeof((x)[0]))

static void print_usage( void ) {
    int i = 0;
    fprintf( stdout, "usage: %s [ ", basename( p_name ) );
    while ( common_options[i].name != NULL ) {
        fprintf( stdout, "--%s ", common_options[i].name );
        if ( common_options[i].has_arg == required_argument )
            fprintf( stdout, "<arg> " );
        if ( unlikely( i == ( ARRAY_SIZE(common_options) - 2 ) ) )
            fprintf( stdout, "]\n");
        else
            fprintf( stdout, " | ");
        i++;
    }
    fprintf( stdout, "target [-t] <cmd> <sub args>:  exec subcommand. Available options are:\n");
    fprintf( stdout, "  cmd = switchpart\n" );
    fprintf( stdout, "      --initialize [-i]     reset the system data memory region\n" );
    fprintf( stdout, "      --bootok [-b]         set the current boot as valid (reset boot fail counter)\n" );
    fprintf( stdout, "      --switch [-s]         perform switch partition\n" );
    fprintf( stdout, "      --counter [-c] 0|1    enable (1) or disable (0) the boot fail counter\n" );
    fprintf( stdout, "      --print [-p]          print the current switch partition setting\n" );
    fprintf( stdout, "\n" );
    fprintf( stdout, "  help [-h]: print this helper\n" );
    fprintf( stdout, "\n" );
    fprintf( stdout, "  version [-v]: print current application version\n" );
    fprintf( stdout, "\n" );
}


static void parse_common_args( int args, char *argv[] ) {
    int c;

    _cmd = CMD_SWITCHPART_NONE;
    while ( ( c = getopt_long( args, argv, ":t:hv", common_options, NULL ) ) != EOF ) {
		switch ( c ) {
            case 't':
                if ( strcmp( optarg, "switchpart") == 0 ) {
                    _cmd = CMD_SWITCHPART;
                } else {
                    fprintf( stderr, "Error: invalid command type!!!\n" );
                    print_usage( );
                    exit(EXIT_FAILURE);
                }
                break;
            case 'h':
                print_usage( );
                exit(EXIT_SUCCESS);
                break;
            case 'v':
                fprintf( stderr, "Compiled with 000 \n" );
                exit(EXIT_SUCCESS);
                break;
            default:
                /* ignore unknown options */
                break;
		}
	}

    /* Reset getopt for the next pass. */
	opterr = 1;
	optind = 1;
}


static void parse_switchpart_args( int args, char *argv[] ) {
    int c;

    while ( ( c = getopt_long( args, argv, ":c:ibsp", switchpart_options, NULL ) ) != EOF ) {

        if ( _cmd_swichpart != CMD_SWITCHPART_NONE )
            break;
       
		switch ( c ) {
            case 'i':
                _cmd_swichpart = CMD_SWITCHPART_INIT;
                break;
            case 'b':
                _cmd_swichpart = CMD_SWITCHPART_BOOTOK;
                break;
            case 's':
                _cmd_swichpart = CMD_SWITCHPART_SWITCH;
                break;
            case 'p':
                _cmd_swichpart = CMD_SWITCHPART_PRINT;
                break;
            case 'c':
                if ( strcmp( optarg, "1") == 0 ) {
                    _cmd_swichpart = CMD_SWITCHPART_COUNTER_EN;
                } else if ( strcmp( optarg, "0") == 0 ) {
                    _cmd_swichpart = CMD_SWITCHPART_COUNTER_DIS;
                } else {
                    fprintf( stderr, "Error: invalid command args!!!\n" );
                    print_usage( );
                    exit(EXIT_FAILURE);
                }        
                break;     
            default:
                /* ignore unknown options */
                break;
		}
	}

    /* Reset getopt for the next pass. */
	opterr = 1;
	optind = 1;
}

/*  _____________________________________________________________________________
 * |_____________________________________________________________________________|
 */


int main( int argc, char *argv[] ) {
    int ret, i;
    uint32_t *counters;

    strcpy( &p_name[0], argv[0] );
    parse_common_args( argc, argv );
    
    if ( _cmd == CMD_NONE ) {

        fprintf( stderr, "Error: command type not defined!!!\n" );
        exit(EXIT_FAILURE);

    } else if ( _cmd == CMD_SWITCHPART ) {

        parse_switchpart_args( argc, argv );
        switch ( _cmd_swichpart ) {
            case CMD_SWITCHPART_INIT:
                fprintf( stdout, "Switchpart: data initialization... " );
                ret = sysdata_init( );
                if ( ret == 0 ) {
                    fprintf( stdout, "OK\n" );
                } else {
                    fprintf( stdout, "error (%d)\n", ret );
                }
                break;
            case CMD_SWITCHPART_BOOTOK:
                fprintf( stdout, "Switchpart: validate boot... " );
                ret = sysdata_switchpart_boot_ok( );
                 if ( ret == 0 ) {
                    fprintf( stdout, "OK\n" );
                } else {
                    fprintf( stdout, "error (%d)\n", ret );
                }
                break;
            case CMD_SWITCHPART_SWITCH:
                fprintf( stdout, "Switchpart: switch partition... " );
                ret = switch_part_group( SWITCH_PART_GROUP_ROUND_WAY );
                if ( ret > 0 ) {
                    fprintf( stdout, "OK (group %d)\n", ret );
                } else {
                    fprintf( stdout, "error (%d)\n", ret );
                }
                break;
            case CMD_SWITCHPART_COUNTER_EN:
                ret = sysdata_switchpart_enable_counter( 1 );
                if ( ret == 0 ) {
                    fprintf( stdout, "OK (boot fail counter enabled)\n" );
                } else {
                    fprintf( stdout, "error (%d)\n", ret );
                }
                break;
            case CMD_SWITCHPART_COUNTER_DIS:
                ret = sysdata_switchpart_enable_counter( 0 );
                if ( ret == 0 ) {
                    fprintf( stdout, "OK (boot fail counter disabled)\n" );
                } else {
                    fprintf( stdout, "error (%d)\n", ret );
                }
                break;
            case CMD_SWITCHPART_PRINT:
                fprintf( stdout, "Switchpart: current state:\n" );
                
                switching_table_t switch_data;
                ret = get_switchpart_data( &switch_data );
                 if ( ret )
                    printf( "Error (%d) on retrieving data\n", ret );
                else {
                    /* since the boot_partition's groups are few
                       we use a pointer in order to avoid aggressive-loop-optimizations
                    */
                    counters = &switch_data.boot_partition_counter[0];
                    for ( i = 0 ; i <= SYSDATA_MAX_PART_GROUP ; i++, counters++ ) {
                        printf( "   Boot on partition group %d: %05d\n", i, *counters );
                    }
                    printf( "   Current boot partition group: %01d\n", switch_data.current_boot_partition );
                    printf( "   Previous boot partition group: %01d\n", switch_data.previous_boot_partition );
                    printf( "   Consecutive fail boot: %01d\n", switch_data.boot_fail_counter );
                    printf( "Boot fail counter state: %s\n", switch_data.enable_counter ? "enabled" : "disabled" );
                }

                break;
            default:
                fprintf( stderr, "Switchpart: invalid command!\n" );
                break;
        }

    }
    return 0;
}
